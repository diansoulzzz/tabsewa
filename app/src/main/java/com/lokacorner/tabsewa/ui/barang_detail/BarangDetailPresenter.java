package com.lokacorner.tabsewa.ui.barang_detail;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.network.model.BarangRequest;
import com.lokacorner.tabsewa.data.network.model.BarangResponse;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class BarangDetailPresenter<V extends BarangDetailMvpView> extends BasePresenter<V>
        implements BarangDetailMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public BarangDetailPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized(Integer posting_id) {
        getMvpView().showLoading();
        getResourceData(posting_id);
    }

    @Override
    public void getResourceData(Integer posting_id) {
        getCompositeDisposable().add(getDataManager()
                .getBarangDataResponse(new BarangRequest.BarangDataRequest(posting_id))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.BarangDataResponse>() {
                    @Override
                    public void accept(@NonNull BarangResponse.BarangDataResponse barangDataResponse)
                            throws Exception {
                        Log.d(TAG,"accept");
                        if (barangDataResponse != null && barangDataResponse.getData() != null) {
                            Log.d(TAG,"accept in");
                            getMvpView().setUpSlider(barangDataResponse.getData().getBarangFotos());
                            getMvpView().setUpBarang(barangDataResponse.getData());
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG,"fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
//                        getMvpView().setUpAdapter();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
