package com.lokacorner.tabsewa.ui.barang_detail;

import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangFoto;
import com.lokacorner.tabsewa.ui.base.MvpView;

import java.util.List;

public interface BarangDetailMvpView extends MvpView {

    void openMainActivity();

    void setUpSlider(List<BarangFoto> barangList);

    void setUpBarang(Barang barang);
}
