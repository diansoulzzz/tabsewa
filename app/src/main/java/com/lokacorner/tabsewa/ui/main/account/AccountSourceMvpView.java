package com.lokacorner.tabsewa.ui.main.account;


import android.opengl.Visibility;

import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.data.db.model.User;
import com.lokacorner.tabsewa.ui.base.MvpView;

import java.util.List;

public interface AccountSourceMvpView extends MvpView {
    void hideMenuLayout();

    void showMenuLayout();

    void setUserInfomation(User user);

//    void startAuthActivity(Integer tab);
}
