package com.lokacorner.tabsewa.ui.auth.login;


import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.ui.base.MvpView;

import java.util.List;

public interface LoginMvpView extends MvpView {

    void openMainActivity();
}
