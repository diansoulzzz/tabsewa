package com.lokacorner.tabsewa.ui.search_filter;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.network.model.BarangResponse;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class SearchResultPresenter<V extends SearchResultMvpView> extends BasePresenter<V>
        implements SearchResultMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public SearchResultPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null) {

        }

    }

    @Override
    public void onViewInitialized() {
        getMvpView().showLoading();
        getResourceData();
    }

    @Override
    public void getResourceData() {
        getCompositeDisposable().add(getDataManager()
                .getBarangListResponse()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.BarangListResponse>() {
                    @Override
                    public void accept(@NonNull BarangResponse.BarangListResponse barangListResponse)
                            throws Exception {
                        Log.d(TAG, "accept");
                        if (barangListResponse != null && barangListResponse.getData() != null) {
                            Log.d(TAG, "accept in");
                            getMvpView().setUpAdapter();
                            getMvpView().addItemAdapterBarang(barangListResponse.getData());
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                        getMvpView().setUpAdapter();
                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
