package com.lokacorner.tabsewa.ui.auth.register;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface RegisterMvpPresenter<V extends RegisterMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();

    void getSliderView();
}
