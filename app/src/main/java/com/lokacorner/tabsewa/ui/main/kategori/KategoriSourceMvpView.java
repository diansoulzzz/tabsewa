package com.lokacorner.tabsewa.ui.main.kategori;


import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.data.db.model.User;
import com.lokacorner.tabsewa.ui.base.MvpView;

import java.util.List;

public interface KategoriSourceMvpView extends MvpView {
    void setUpDrawer(List<BarangKategori> barangKategoriList);
    void setUpSubKategori();

}
