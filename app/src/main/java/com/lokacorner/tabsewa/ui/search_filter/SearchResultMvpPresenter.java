package com.lokacorner.tabsewa.ui.search_filter;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface SearchResultMvpPresenter<V extends SearchResultMvpView> extends MvpPresenter<V> {

    void onViewInitialized();

    void getResourceData();

}
