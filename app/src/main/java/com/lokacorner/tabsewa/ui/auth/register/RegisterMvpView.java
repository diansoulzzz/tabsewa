package com.lokacorner.tabsewa.ui.auth.register;


import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.ui.base.MvpView;

import java.util.List;

public interface RegisterMvpView extends MvpView {

    void setUpAdapter();

    void updateLatest(List<Barang> barangList);

    void setUpSlider(List<Barang> barangList);

    void addItemAdapterBarangKategori(List<BarangKategori> objects);

    void addItemAdapterBarang(List<Barang> objects);

    void showHeaderList(Integer visibility);

    void openBarangDetailActivity(Barang barang);
}
