package com.lokacorner.tabsewa.ui.register_vendor;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface RegisterVendorMvpPresenter<V extends RegisterVendorMvpView> extends MvpPresenter<V> {

    void onViewInitialized();

}
