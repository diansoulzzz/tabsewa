package com.lokacorner.tabsewa.ui.empty;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface EmptyMvpPresenter<V extends EmptyMvpView> extends MvpPresenter<V> {

    void onViewInitialized();

}
