package com.lokacorner.tabsewa.ui.main.home;

import android.content.Context;
import android.graphics.Color;
import android.opengl.Visibility;
import android.os.Bundle;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.network.model.BarangResponse;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class HomeSourcePresenter<V extends HomeSourceMvpView> extends BasePresenter<V>
        implements HomeSourceMvpPresenter<V> {

    private static final String TAG = "SearchSourcePresenter";
    @Inject
    public HomeSourcePresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getMvpView().showLoading();
        getResourceData();
    }
    @Override
    public void getResourceData() {
        getCompositeDisposable().add(getDataManager()
                .getHomeDataResponse()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Log.d(TAG,"fail");
//                        getMvpView().hideLoading();
//                    }
//                })
                .subscribe(new Consumer<HomeResponse.HomeDataResponse>() {
                    @Override
                    public void accept(@NonNull HomeResponse.HomeDataResponse homeDataResponse)
                            throws Exception {
                        Log.d(TAG,"accept");
                        if (homeDataResponse != null && homeDataResponse.getData() != null) {
                            Log.d(TAG,"accept in");
                            getMvpView().setUpSlider(homeDataResponse.getData().getCarousel());
                            getMvpView().showHeaderList(0);
                            getMvpView().setUpAdapter();
                            getMvpView().addItemAdapterBarang(homeDataResponse.getData().getBarangList());
                            getMvpView().addItemAdapterBarangKategori(homeDataResponse.getData().getBarangKategoriList());
                        }
                        getMvpView().hideLoading();
//                        getCompositeDisposable().dispose();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG,"fail");
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();
                        getMvpView().setUpAdapter();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
    @Override
    public void getSliderView(){
//        ArrayList<String> listUrl = new ArrayList<>();
//        ArrayList<String> listName = new ArrayList<>();
//        listUrl.add("https://www.revive-adserver.com/media/GitHub.jpg");
//        listName.add("JPG - Github");
//        listUrl.add("https://tctechcrunch2011.files.wordpress.com/2017/02/android-studio-logo.png");
//        listName.add("PNG - Android Studio");
//        listUrl.add("http://static.tumblr.com/7650edd3fb8f7f2287d79a67b5fec211/3mg2skq/3bdn278j2/tumblr_static_idk_what.gif");
//        listName.add("GIF - Disney");
//        listUrl.add("http://www.gstatic.com/webp/gallery/1.webp");
//        listName.add("WEBP - Mountain");
//        listUrl.add("https://www.clicktorelease.com/tmp/google-svg/image.svg");
//        listName.add("SVG - Google");

    }
}
