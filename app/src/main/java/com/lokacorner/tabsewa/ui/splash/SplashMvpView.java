package com.lokacorner.tabsewa.ui.splash;


import com.lokacorner.tabsewa.ui.base.MvpView;


public interface SplashMvpView extends MvpView {

    void openLoginActivity();

    void openMainActivity();

    void startSyncService();
}
