package com.lokacorner.tabsewa.ui.image_detail;

import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class ImageDetailPresenter<V extends ImageDetailMvpView> extends BasePresenter<V>
        implements ImageDetailMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public ImageDetailPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized() {

    }
}
