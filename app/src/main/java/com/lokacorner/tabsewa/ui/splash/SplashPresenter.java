package com.lokacorner.tabsewa.ui.splash;


import com.google.firebase.auth.FirebaseUser;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;
import javax.inject.Inject;
import io.reactivex.ObservableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;


public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {

    @Inject
    public SplashPresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getMvpView().startSyncService();
        getMvpView().openMainActivity();
//        decideNextActivity();
//        getCompositeDisposable().add(getDataManager()
//                .seedDatabaseQuestions()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .concatMap(new Function<Boolean, ObservableSource<Boolean>>() {
//                    @Override
//                    public ObservableSource<Boolean> apply(Boolean aBoolean) throws Exception {
//                        return getDataManager().seedDatabaseOptions();
//                    }
//                })
//                .subscribe(new Consumer<Boolean>() {
//                    @Override
//                    public void accept(Boolean aBoolean) throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        decideNextActivity();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        getMvpView().onError(R.string.some_error);
//                        decideNextActivity();
//                    }
//                }));
    }

    private void decideNextActivity() {
//        getMvpView().openMainActivity();
//        getMvpView().openLoginActivity();
//
        FirebaseUser user = getFirebaseAuth().getCurrentUser();
        if (user != null) {
            getMvpView().openMainActivity();
        }
        else
        {
            getMvpView().openLoginActivity();
        }
//        if (getDataManager().getCurrentUserLoggedInMode()
//                == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
//            getMvpView().openLoginActivity();
//        } else {
//            getMvpView().openMainActivity();
//        }
    }
}
