package com.lokacorner.tabsewa.ui.search;

import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class SearchPresenter<V extends SearchMvpView> extends BasePresenter<V>
        implements SearchMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public SearchPresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized() {

    }
}
