package com.lokacorner.tabsewa.ui.image_detail;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface ImageDetailMvpPresenter<V extends ImageDetailMvpView> extends MvpPresenter<V> {

    void onViewInitialized();
}
