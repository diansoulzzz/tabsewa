package com.lokacorner.tabsewa.ui.main.account.vendor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.di.component.ActivityComponent;
import com.lokacorner.tabsewa.ui.base.BaseFragment;
import com.lokacorner.tabsewa.ui.main.MainMvpView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VendorSourceFragment extends BaseFragment implements
        VendorSourceMvpView {

    private static final String TAG = "VendorSourceFragment";

    @Inject
    VendorSourceMvpPresenter<VendorSourceMvpView> mPresenter;

    public static VendorSourceFragment newInstance() {
        Bundle args = new Bundle();
        VendorSourceFragment fragment = new VendorSourceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_tab_vendor, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.onViewPrepared();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
