package com.lokacorner.tabsewa.ui.main.account.vendor;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface VendorSourceMvpPresenter<V extends VendorSourceMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();
}
