package com.lokacorner.tabsewa.ui.image_detail;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface ImageDetailMvpView extends MvpView {

    void openMainActivity();
}
