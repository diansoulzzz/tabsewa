package com.lokacorner.tabsewa.ui.auth.loginold;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface LoginOldMvpView extends MvpView {

    void openMainActivity();
}
