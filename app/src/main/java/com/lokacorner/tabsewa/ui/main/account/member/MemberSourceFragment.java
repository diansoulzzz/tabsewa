package com.lokacorner.tabsewa.ui.main.account.member;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.User;
import com.lokacorner.tabsewa.di.component.ActivityComponent;
import com.lokacorner.tabsewa.ui.base.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberSourceFragment extends BaseFragment implements
        MemberSourceMvpView {

    private static final String TAG = "MemberSourceFragment";

    @Inject
    MemberSourceMvpPresenter<MemberSourceMvpView> mPresenter;

    public static MemberSourceFragment newInstance() {
        Bundle args = new Bundle();
        MemberSourceFragment fragment = new MemberSourceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_tab_member, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.onViewPrepared();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void setMemberInfo(User user) {
    }
}
