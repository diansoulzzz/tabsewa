package com.lokacorner.tabsewa.ui.main.kategori;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.network.model.CategoryResponse;
import com.lokacorner.tabsewa.data.network.model.UserResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class KategoriSourcePresenter<V extends KategoriSourceMvpView> extends BasePresenter<V>
        implements KategoriSourceMvpPresenter<V> {

    private static final String TAG = "KategoriPresenter";

    @Inject
    public KategoriSourcePresenter(DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getMvpView().showLoading();
        getResourceData();
    }

    @Override
    public void getResourceData() {
        Log.d(TAG, "start accept");
        getCompositeDisposable().add(getDataManager()
                .getCategoryListResponse()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CategoryResponse.CategoryListResponse>() {
                    @Override
                    public void accept(@NonNull CategoryResponse.CategoryListResponse categoryListResponse) throws Exception {
                        Log.d(TAG, "accept");
                        if (categoryListResponse != null && categoryListResponse.getData() != null) {
                            Log.d(TAG, "accept in");
                            getMvpView().setUpDrawer(categoryListResponse.getData());
                        }
                        getMvpView().hideLoading();
//                        getCompositeDisposable().dispose();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
