package com.lokacorner.tabsewa.ui.main;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.ui.base.BaseActivity;
import com.lokacorner.tabsewa.ui.main.account.AccountSourceFragment;
import com.lokacorner.tabsewa.ui.main.home.HomeSourceFragment;
import com.lokacorner.tabsewa.ui.main.kategori.KategoriSourceFragment;
import com.lokacorner.tabsewa.ui.search.SearchActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @BindView(R.id.bottomNavigationViewEx)
    BottomNavigationViewEx bottomNavigationViewEx;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private static final String TAG = "BarangDetailActivity";
    private boolean firsttime = true;
    private float appBarElevation = 0;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(MainActivity.this);
        Log.d(TAG, "Started Here");
        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bottomNavigationViewEx.enableAnimation(true);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        fragmentManager = getSupportFragmentManager();
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == bottomNavigationViewEx.getSelectedItemId() && !firsttime)
                    return false;
                transaction = fragmentManager.beginTransaction();
                switch (item.getItemId()) {
                    case R.id.menu_home:
                        transaction.replace(R.id.content, new HomeSourceFragment()).commit();
                        return true;
                    case R.id.menu_category:
                        transaction.replace(R.id.content, new KategoriSourceFragment()).commit();
                        return true;
                    case R.id.menu_account:
                        transaction.replace(R.id.content, new AccountSourceFragment()).commit();
                        return true;
                }
                return false;
            }
        });
        bottomNavigationViewEx.setCurrentItem(0);
        firsttime = false;
    }


    @OnClick(R.id.btnSearch)
    void goToSearch() {
        openSearchActivity();
    }

    @Override
    public void openSearchActivity() {
        Intent intent = SearchActivity.getStartIntent(this);
        startActivity(intent);
    }

    @Override
    public void openLoginActivity() {
        Toast.makeText(this, "AB", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void appBarLayoutHideElevation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (appBarLayout != null) {
                appBarElevation = appBarLayout.getElevation();
                appBarLayout.setElevation(0);
            }
        }
    }

    @Override
    public void appBarLayoutShowElevation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (appBarLayout != null) {
                appBarLayout.setElevation(appBarElevation);
            }
        }
    }

    @Override
    public void setBottomMenuIndex(Integer index) {
        bottomNavigationViewEx.setCurrentItem(index);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
