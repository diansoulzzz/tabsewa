package com.lokacorner.tabsewa.ui.search_filter;

import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.ui.base.MvpView;

import java.util.List;

public interface SearchResultMvpView extends MvpView {

    void setUpAdapter();

    void addItemAdapterBarang(List<Barang> barang);

    void openSearchActivity();

    void openBarangDetailActivity(Barang barang);
}
