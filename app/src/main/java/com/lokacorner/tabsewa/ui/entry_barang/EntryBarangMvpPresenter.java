package com.lokacorner.tabsewa.ui.entry_barang;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface EntryBarangMvpPresenter<V extends EntryBarangMvpView> extends MvpPresenter<V> {

    void onViewInitialized();

}
