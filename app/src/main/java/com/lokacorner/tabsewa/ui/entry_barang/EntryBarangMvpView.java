package com.lokacorner.tabsewa.ui.entry_barang;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface EntryBarangMvpView extends MvpView {

    void openMainActivity();
}
