package com.lokacorner.tabsewa.ui.main.search;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class SearchSourcePresenter<V extends SearchSourceMvpView> extends BasePresenter<V>
        implements SearchSourceMvpPresenter<V> {

    private static final String TAG = "SearchSourcePresenter";
    @Inject
    public SearchSourcePresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getMvpView().showLoading();
        getResourceData();
    }
    @Override
    public void getResourceData() {
        getCompositeDisposable().add(getDataManager()
                .getHomeDataResponse()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HomeResponse.HomeDataResponse>() {
                    @Override
                    public void accept(@NonNull HomeResponse.HomeDataResponse homeDataResponse)
                            throws Exception {
                        Log.d(TAG,"accept");
                        if (homeDataResponse != null && homeDataResponse.getData() != null) {
                            Log.d(TAG,"accept in");
                            getMvpView().setUpSlider(homeDataResponse.getData().getBarangList());

                            getMvpView().showHeaderList(0);
                            getMvpView().setUpAdapter();
                            getMvpView().addItemAdapterBarang(homeDataResponse.getData().getBarangList());
                            getMvpView().addItemAdapterBarangKategori(homeDataResponse.getData().getBarangKategoriList());
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG,"fail");
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();
                        getMvpView().setUpAdapter();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
    @Override
    public void getSliderView(){
        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();
        listUrl.add("https://www.revive-adserver.com/media/GitHub.jpg");
        listName.add("JPG - Github");
        listUrl.add("https://tctechcrunch2011.files.wordpress.com/2017/02/android-studio-logo.png");
        listName.add("PNG - Android Studio");
        listUrl.add("http://static.tumblr.com/7650edd3fb8f7f2287d79a67b5fec211/3mg2skq/3bdn278j2/tumblr_static_idk_what.gif");
        listName.add("GIF - Disney");
        listUrl.add("http://www.gstatic.com/webp/gallery/1.webp");
        listName.add("WEBP - Mountain");
        listUrl.add("https://www.clicktorelease.com/tmp/google-svg/image.svg");
        listName.add("SVG - Google");

    }
}
