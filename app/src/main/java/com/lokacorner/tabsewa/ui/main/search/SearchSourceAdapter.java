package com.lokacorner.tabsewa.ui.main.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.ui.base.BaseViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchSourceAdapter {

    private SearchSourceAdapter() {
        // This class is not publicly instantiable
    }


    public interface Callback {
        void onBarangEmptyViewRetryClick();
    }

    public static class HomeSourceAdapterBarang extends RecyclerView.Adapter<BaseViewHolder> {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_BARANG = 1;

        private List<Barang> barangs;

        private Callback mCallback;

        public void setCallback(Callback callback) {
            mCallback = callback;
        }

        public HomeSourceAdapterBarang(List<Barang> barangs) {
            this.barangs = barangs;
        }

        @Override
        public void onBindViewHolder(BaseViewHolder holder, int position) {
            holder.onBind(position);
        }

        @Override
        public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_BARANG:
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang, parent, false);
                    view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                    return new ViewRecommend(view);
                case VIEW_TYPE_EMPTY:
                default:
                    View empty = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false);
                    return new EmptyViewHolder(empty);
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (this.barangs != null && this.barangs.size() > 0) {
                return VIEW_TYPE_BARANG;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        @Override
        public int getItemCount() {
            if (this.barangs != null && this.barangs.size() > 0) {
                return this.barangs.size();
            } else {
                return 1;
            }
        }

        public void clear() {
            this.barangs.clear();
            notifyDataSetChanged();
        }

        public void addItems(List<Barang> barang) {
            this.barangs.addAll(barang);
            notifyDataSetChanged();
        }

        public class ViewRecommend extends BaseViewHolder {

            @BindView(R.id.cover_image_view)
            ImageView coverImageView;

            @BindView(R.id.title_text_view)
            TextView titleTextView;

            @BindView(R.id.content_text_view)
            TextView contentTextView;

            public ViewRecommend(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            protected void clear() {
                coverImageView.setImageDrawable(null);
                titleTextView.setText("");
                contentTextView.setText("");
            }

            public void onBind(int position) {
                super.onBind(position);
                final Barang barang = barangs.get(position);

                if (barang.getNama() != null) {
                    titleTextView.setText(barang.getNama());
                }

                if (barang.getNama() != null) {
                    contentTextView.setText(barang.getNama());
                }

                Glide.with(itemView)
                        .load(barang.getBarangFotoUtama().getFotoUrl())
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true))
                        .into(coverImageView);
            }
        }

        public class EmptyViewHolder extends BaseViewHolder {

            @BindView(R.id.btn_retry)
            Button retryButton;

            @BindView(R.id.tv_message)
            TextView messageTextView;

            public EmptyViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            @OnClick(R.id.btn_retry)
            void onRetryClick() {
                if (mCallback != null)
                    mCallback.onBarangEmptyViewRetryClick();
            }
        }
    }

    public static class HomeSourceAdapterBarangKategori extends RecyclerView.Adapter<BaseViewHolder> {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_KATEGORI = 1;

        private Callback mCallback;

        public void setCallback(Callback callback) {
            mCallback = callback;
        }

        private List<BarangKategori> barangKategoris;

        public HomeSourceAdapterBarangKategori(List<BarangKategori> barangKategoris) {
            this.barangKategoris = barangKategoris;
        }

        @Override
        public void onBindViewHolder(BaseViewHolder holder, int position) {
            holder.onBind(position);
        }

        @Override
        public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_KATEGORI:
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
                    view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                    return new ViewCategory(view);
                case VIEW_TYPE_EMPTY:
                default:
                    View empty = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false);
                    return new EmptyViewHolder(empty);
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (this.barangKategoris != null && this.barangKategoris.size() > 0) {
                return VIEW_TYPE_KATEGORI;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        @Override
        public int getItemCount() {
            if (this.barangKategoris != null && this.barangKategoris.size() > 0) {
                return this.barangKategoris.size();
            } else {
                return 1;
            }
        }

        public void clear() {
            this.barangKategoris.clear();
            notifyDataSetChanged();
        }

        public void addItems(List<BarangKategori> barangKategoris) {
            this.barangKategoris.addAll(barangKategoris);
            notifyDataSetChanged();
        }

        public class ViewCategory extends BaseViewHolder {

            @BindView(R.id.cover_image_view)
            ImageView coverImageView;

            @BindView(R.id.title_text_view)
            TextView titleTextView;

            @BindView(R.id.content_text_view)
            TextView contentTextView;

            public ViewCategory(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            protected void clear() {
                coverImageView.setImageDrawable(null);
                titleTextView.setText("");
                contentTextView.setText("");
            }

            public void onBind(int position) {
                super.onBind(position);
                final BarangKategori barangKategori = barangKategoris.get(position);

                if (barangKategori.getNama() != null) {
                    titleTextView.setText(barangKategori.getNama());
                }

                if (barangKategori.getNama() != null) {
                    contentTextView.setText(barangKategori.getNama());
                }
                Glide.with(itemView)
                        .load(barangKategori.getFotoUrl())
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true))
                        .into(coverImageView);
            }
        }

        public class EmptyViewHolder extends BaseViewHolder {

            @BindView(R.id.btn_retry)
            Button retryButton;

            @BindView(R.id.tv_message)
            TextView messageTextView;

            public EmptyViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            @OnClick(R.id.btn_retry)
            void onRetryClick() {
                if (mCallback != null)
                    mCallback.onBarangEmptyViewRetryClick();
            }
        }
    }
}
