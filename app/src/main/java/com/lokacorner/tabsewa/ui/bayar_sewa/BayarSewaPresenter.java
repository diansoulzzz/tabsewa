package com.lokacorner.tabsewa.ui.bayar_sewa;

import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class BayarSewaPresenter<V extends BayarSewaMvpView> extends BasePresenter<V>
        implements BayarSewaMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public BayarSewaPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized() {

    }
}
