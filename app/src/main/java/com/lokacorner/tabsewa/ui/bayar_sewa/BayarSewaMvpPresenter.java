package com.lokacorner.tabsewa.ui.bayar_sewa;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface BayarSewaMvpPresenter<V extends BayarSewaMvpView> extends MvpPresenter<V> {

    void onViewInitialized();
}
