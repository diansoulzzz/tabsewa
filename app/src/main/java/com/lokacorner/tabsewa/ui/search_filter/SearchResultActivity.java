package com.lokacorner.tabsewa.ui.search_filter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.ui.barang_detail.BarangDetailActivity;
import com.lokacorner.tabsewa.ui.base.BaseActivity;
import com.lokacorner.tabsewa.ui.search.SearchActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SearchResultActivity extends BaseActivity implements SearchResultMvpView, SearchResultAdapter.Callback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    GridLayoutManager mGridLayoutManager;

    @BindView(R.id.result_search_view)
    RecyclerView mResultView;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @Inject
    SearchResultAdapter.SearchAdapterBarang mSearchAdapterBarang;

    @BindView(R.id.btnSearch)
    Button btnSearch;
    @Inject
    SearchResultMvpPresenter<SearchResultMvpView> mPresenter;
    private static final String TAG = "SearchResultActivity";
    private RecyclerView mRecyclerView;
    //    private SearchResultAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    //    private SearchView mSearchView;
    private String mQuery;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SearchResultActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(SearchResultActivity.this);
        mSearchAdapterBarang.setCallback(this);
        Log.d(TAG, "Started Here");
        setUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.toolbar_search, menu);
//        MenuItem searchItem = menu.findItem(R.id.menu_search);
//        mSearchView = (SearchView) searchItem.getActionView();
//        setupSearchView(searchItem);
//        if (mQuery != null) {
//            mSearchView.setQuery(mQuery, false);
//        }
//        return true;
//    }


    @OnClick(R.id.btnSearch)
    void goToSearch() {
        this.finish();
    }

    @Override
    public void openSearchActivity() {
        Intent intent = SearchActivity.getStartIntent(this);
        intent.putExtra("query", mQuery);
        startActivity(intent);
    }


    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            swiperefresh.setRefreshing(false);
            mPresenter.onViewInitialized();
        }
    };

    @Override
    protected void setUp() {
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            mQuery = null;
        } else {
            mQuery = extras.getString("query");
            btnSearch.setText(mQuery);
        }
        swiperefresh.setOnRefreshListener(onRefreshListener);
        mGridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        mResultView.setLayoutManager(mGridLayoutManager);
        mResultView.setItemAnimator(new DefaultItemAnimator());
        mPresenter.onViewInitialized();
    }

    @Override
    public void setUpAdapter() {
        mResultView.setAdapter(mSearchAdapterBarang);
        mSearchAdapterBarang.clear();
    }

    @Override
    public void addItemAdapterBarang(List<Barang> barangs) {
        mSearchAdapterBarang.addItems(barangs);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onBarangEmptyViewRetryClick() {

    }

    @Override
    public void onBarangDetailClick(Barang barang) {
        openBarangDetailActivity(barang);
    }

    @Override
    public void openBarangDetailActivity(Barang barang) {
        Intent intent = BarangDetailActivity.getStartIntent(this);
        intent.putExtra("barang_id", barang.getId());
        startActivity(intent);
    }
}
