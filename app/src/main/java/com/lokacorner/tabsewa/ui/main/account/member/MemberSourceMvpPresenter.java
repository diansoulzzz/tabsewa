package com.lokacorner.tabsewa.ui.main.account.member;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface MemberSourceMvpPresenter<V extends MemberSourceMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();
}
