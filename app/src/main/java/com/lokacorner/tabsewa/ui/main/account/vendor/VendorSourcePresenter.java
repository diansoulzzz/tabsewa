package com.lokacorner.tabsewa.ui.main.account.vendor;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class VendorSourcePresenter<V extends VendorSourceMvpView> extends BasePresenter<V>
        implements VendorSourceMvpPresenter<V> {

    private static final String TAG = "VendorSourcePresenter";

    @Inject
    public VendorSourcePresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
//        getMvpView().showLoading();
//        getResourceData();
    }

    @Override
    public void getResourceData() {
        getCompositeDisposable().add(getDataManager()
                .getHomeDataResponse()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HomeResponse.HomeDataResponse>() {
                    @Override
                    public void accept(@NonNull HomeResponse.HomeDataResponse homeDataResponse)
                            throws Exception {
                        Log.d(TAG, "accept");
                        if (homeDataResponse != null && homeDataResponse.getData() != null) {
                            Log.d(TAG, "accept in");

                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
