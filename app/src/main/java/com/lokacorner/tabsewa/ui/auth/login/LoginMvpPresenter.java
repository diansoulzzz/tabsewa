package com.lokacorner.tabsewa.ui.auth.login;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginMvpView>
        extends MvpPresenter<V> {

    void onServerLoginClick(String email, String password);

    void onGoogleRequestToken(String uid,String token);

    void onFacebookRequestToken(String uid,String token);

    void onFacebookAuth(AccessToken accessToken);

    void onGoogleAuth(GoogleSignInAccount googleSignInAccount);
}
