package com.lokacorner.tabsewa.ui.main.account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.User;
import com.lokacorner.tabsewa.di.component.ActivityComponent;
import com.lokacorner.tabsewa.ui.base.BaseFragment;
import com.lokacorner.tabsewa.ui.main.MainMvpView;
import com.lokacorner.tabsewa.ui.main.account.member.MemberSourceFragment;
import com.lokacorner.tabsewa.ui.main.account.vendor.VendorSourceFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountSourceFragment extends BaseFragment implements
        AccountSourceMvpView {

    private static final String TAG = "SearchSourceFragment";

    @Inject
    AccountSourceMvpPresenter<AccountSourceMvpView> mPresenter;

    private SectionsPagerAdapter mSectionsPagerAdapter;

//    @BindView(R.id.swiperefresh)
//    SwipeRefreshLayout swiperefresh;
//
//    @BindView(R.id.refreshLayout)
//    RefreshLayout refreshLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.imgAccount)
    ImageView imgAccount;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtInfo)
    TextView txtInfo;
    @BindView(R.id.layout_auth)
    LinearLayout layout_auth;

    private MainMvpView listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MainMvpView) context;
        } catch (ClassCastException castException) {
            /* The activity does not implement the listener. */
        }
    }

    public static AccountSourceFragment newInstance() {
        Bundle args = new Bundle();
        AccountSourceFragment fragment = new AccountSourceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                mPresenter.setUserAsLoggedOut();
                restartApp();
//                listener.setBottomMenuIndex(0);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
//        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_account, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            setHasOptionsMenu(true);
        }
        return view;
    }
//
//    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
//        @Override
//        public void onRefresh() {
//            swiperefresh.setRefreshing(false);
//            mPresenter.onViewPrepared();
//        }
//    };

    @Override
    protected void setUp(View view) {
//        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(@NonNull RefreshLayout refreshlayout) {
//                refreshlayout.finishRefresh();
//                mPresenter.onViewPrepared();
//            }
//        });
//        swiperefresh.setOnRefreshListener(onRefreshListener);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(mSectionsPagerAdapter);
//        setHasOptionsMenu(true);
        tabLayout.setupWithViewPager(viewPager);
        mPresenter.onViewPrepared();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        listener.appBarLayoutHideElevation();
        super.onStart();
    }

    @Override
    public void onDestroy() {
        listener.appBarLayoutShowElevation();
        super.onDestroy();
    }

    @Override
    public void hideMenuLayout() {
        tabLayout.setVisibility(View.GONE);
//        txtInfo.setVisibility(View.GONE);
//        txtNama.setVisibility(View.GONE);
        imgAccount.setVisibility(View.GONE);
        layout_auth.setVisibility(View.VISIBLE);
        txtInfo.setText(R.string.hello);
        txtNama.setText(R.string.guest);
    }

    @Override
    public void showMenuLayout() {
        layout_auth.setVisibility(View.GONE);
        tabLayout.setVisibility(View.VISIBLE);
        imgAccount.setVisibility(View.VISIBLE);
        txtInfo.setVisibility(View.VISIBLE);
        txtNama.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserInfomation(User user) {
        Glide.with(this)
                .load(user.getFotoUrl())
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true).circleCrop())
                .into(imgAccount);
        txtInfo.setText(R.string.hello);
        txtNama.setText(user.getName());
    }

//    @Override
//    public void startAuthActivity(Integer tab) {
//
//    }

    @OnClick(R.id.btn_login)
    void startLogin() {
        openAuthActivity(0);
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode,resultCode,data);
//        Toast.makeText(getContext(),"Login",Toast.LENGTH_SHORT).show();
//        if (requestCode == 771) {
//            if(resultCode == 1){
//                listener.setBottomMenuIndex(0);
////                String result= data.getStringExtra("loggedIn");
//            }
//        }
//    }

    @OnClick(R.id.btn_register)
    void startRegister() {
        openAuthActivity(1);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return MemberSourceFragment.newInstance();
                case 1:
                    return VendorSourceFragment.newInstance();
            }
            return MemberSourceFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Member";
                case 1:
                    return "Vendor";
            }
            return null;
        }
    }
}
