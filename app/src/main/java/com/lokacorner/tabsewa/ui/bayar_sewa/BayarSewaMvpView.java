package com.lokacorner.tabsewa.ui.bayar_sewa;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface BayarSewaMvpView extends MvpView {

    void openMainActivity();
}
