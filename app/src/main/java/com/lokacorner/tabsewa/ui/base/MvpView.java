package com.lokacorner.tabsewa.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by GBS on 31/01/2018.
 */

public interface MvpView {

    void showLoading();

    void hideLoading();

    void openActivityOnTokenExpire();

    void openAuthActivity(Integer tab);

    void restartApp();
    
    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

}
