package com.lokacorner.tabsewa.ui.entry_barang;

import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class EntryBarangPresenter<V extends EntryBarangMvpView> extends BasePresenter<V>
        implements EntryBarangMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public EntryBarangPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized() {

    }
}
