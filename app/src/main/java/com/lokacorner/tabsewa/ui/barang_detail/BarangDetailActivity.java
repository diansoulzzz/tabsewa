package com.lokacorner.tabsewa.ui.barang_detail;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangFoto;
import com.lokacorner.tabsewa.ui.auth.AuthActivity;
import com.lokacorner.tabsewa.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class BarangDetailActivity extends BaseActivity implements BarangDetailMvpView, BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    @Inject
    BarangDetailMvpPresenter<BarangDetailMvpView> mPresenter;
    private static final String TAG = "BarangDetailActivity";
    Integer posting_id;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnbar_layout)
    ButtonBarLayout btnbar_layout;
    @BindView(R.id.btn_chat)
    Button btn_chat;
    @BindView(R.id.btn_sewa)
    Button btn_sewa;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.slider_layout)
    SliderLayout mSliderLayout;
    @BindView(R.id.txtNamaBarang)
    TextView txtNamaBarang;
    @BindView(R.id.txtNamaPenyewa)
    TextView txtNamaPenyewa;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, BarangDetailActivity.class);
        return intent;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            swiperefresh.setRefreshing(false);
            mPresenter.onViewInitialized(posting_id);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang_detail);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(BarangDetailActivity.this);
        Log.d(TAG, "Started Here");
//        Toast.makeText(this, ""+posting_id, Toast.LENGTH_SHORT).show();
        setUp();
    }

    @Override
    public void setUpSlider(List<BarangFoto> barangFotosList) {
        mSliderLayout.removeAllSliders();
        for (int i = 0; i < barangFotosList.size(); i++) {
//            Toast.makeText(getApplicationContext(),barangFotosList.get(i).getFotoUrl(),Toast.LENGTH_SHORT).show();
            TextSliderView sliderView = new TextSliderView(getApplicationContext());
            sliderView
                    .image(barangFotosList.get(i).getFotoUrl())
                    .setRequestOption(new RequestOptions().centerCrop())
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", barangFotosList.get(i).getFotoUrl());
            mSliderLayout.addSlider(sliderView);
        }
        mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSliderLayout.setCustomAnimation(new DescriptionAnimation());
        mSliderLayout.setDuration(4000);
        mSliderLayout.addOnPageChangeListener(this);
    }

    @Override
    public void setUpBarang(Barang barang) {
        txtNamaBarang.setText(barang.getNama());
        txtNamaPenyewa.setText(barang.getUser().getName());

    }

    @OnClick(R.id.btn_sewa)
    public void onClickSewa(View view) {
        Toast.makeText(this, "Click Sewa", Toast.LENGTH_SHORT).show();
        Intent intent = AuthActivity.getStartIntent(view.getContext());
//        intent.putExtra("barang_id", barang.getId());
        startActivity(intent);
    }

    @OnClick(R.id.btn_chat)
    public void onClickChat(View view) {
        Toast.makeText(this, "Chat", Toast.LENGTH_SHORT).show();
        Intent intent = AuthActivity.getStartIntent(view.getContext());
//        intent.putExtra("barang_id", barang.getId());
        startActivity(intent);
    }

    @Override
    protected void setUp() {
        posting_id = getIntent().getIntExtra("barang_id", 0);
        mPresenter.onViewInitialized(posting_id);
        swiperefresh.setOnRefreshListener(onRefreshListener);
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
        Toast.makeText(getApplicationContext(), baseSliderView.getBundle().get("extra").toString() + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openMainActivity() {

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        mSliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
