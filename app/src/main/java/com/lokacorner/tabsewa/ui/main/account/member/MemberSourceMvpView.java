package com.lokacorner.tabsewa.ui.main.account.member;


import com.lokacorner.tabsewa.data.db.model.User;
import com.lokacorner.tabsewa.ui.base.MvpView;

public interface MemberSourceMvpView extends MvpView {
    void setMemberInfo(User user);
}
