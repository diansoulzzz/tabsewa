package com.lokacorner.tabsewa.ui.register_vendor;

import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class RegisterVendorPresenter<V extends RegisterVendorMvpView> extends BasePresenter<V>
        implements RegisterVendorMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public RegisterVendorPresenter(DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized() {

    }
}
