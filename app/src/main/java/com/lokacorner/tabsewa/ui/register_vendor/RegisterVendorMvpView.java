package com.lokacorner.tabsewa.ui.register_vendor;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface RegisterVendorMvpView extends MvpView {

    void openMainActivity();
}
