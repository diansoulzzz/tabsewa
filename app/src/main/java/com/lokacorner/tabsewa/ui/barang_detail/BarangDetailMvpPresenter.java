package com.lokacorner.tabsewa.ui.barang_detail;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface BarangDetailMvpPresenter<V extends BarangDetailMvpView> extends MvpPresenter<V> {

    void onViewInitialized(Integer posting_id);

    void getResourceData(Integer posting_id);
}
