package com.lokacorner.tabsewa.ui.main.search;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface SearchSourceMvpPresenter<V extends SearchSourceMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();

    void getSliderView();
}
