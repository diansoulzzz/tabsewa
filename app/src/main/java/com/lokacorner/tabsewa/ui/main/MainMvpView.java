package com.lokacorner.tabsewa.ui.main;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void openLoginActivity();

    void appBarLayoutHideElevation();

    void appBarLayoutShowElevation();

    void setBottomMenuIndex(Integer index);

    void openSearchActivity();
}
