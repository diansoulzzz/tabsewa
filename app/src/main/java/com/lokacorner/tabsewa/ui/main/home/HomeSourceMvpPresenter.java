package com.lokacorner.tabsewa.ui.main.home;
import android.content.Context;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface HomeSourceMvpPresenter<V extends HomeSourceMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();

    void getSliderView();
}
