package com.lokacorner.tabsewa.ui.auth;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface AuthMvpView extends MvpView {

    void openMainActivity();
}
