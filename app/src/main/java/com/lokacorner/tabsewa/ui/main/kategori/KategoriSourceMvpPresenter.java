package com.lokacorner.tabsewa.ui.main.kategori;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface KategoriSourceMvpPresenter<V extends KategoriSourceMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();
}
