package com.lokacorner.tabsewa.ui.empty;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface EmptyMvpView extends MvpView {

    void openMainActivity();
}
