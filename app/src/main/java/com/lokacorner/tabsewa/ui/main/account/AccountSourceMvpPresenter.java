package com.lokacorner.tabsewa.ui.main.account;

import com.lokacorner.tabsewa.ui.base.MvpPresenter;

public interface AccountSourceMvpPresenter<V extends AccountSourceMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void getResourceData();
}
