package com.lokacorner.tabsewa.ui.empty;

import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class EmptyPresenter<V extends EmptyMvpView> extends BasePresenter<V>
        implements EmptyMvpPresenter<V> {

    private static final String TAG = "BarangDetailPresenter";

    @Inject
    public EmptyPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        if (getDataManager().getAccessToken() != null)
        {

        }

    }

    @Override
    public void onViewInitialized() {

    }
}
