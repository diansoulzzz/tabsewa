package com.lokacorner.tabsewa.ui.main.account;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.data.network.model.UserResponse;
import com.lokacorner.tabsewa.ui.base.BasePresenter;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class AccountSourcePresenter<V extends AccountSourceMvpView> extends BasePresenter<V>
        implements AccountSourceMvpPresenter<V> {

    private static final String TAG = "AccountPresenter";

    @Inject
    public AccountSourcePresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        if (getFirebaseAuth().getCurrentUser() == null)
        {
            getMvpView().hideMenuLayout();
        }
        else
        {
            getMvpView().showLoading();
            getResourceData();
        }
    }

    @Override
    public void getResourceData() {
        getCompositeDisposable().add(getDataManager()
                .getUserDataResponse()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Log.d(TAG,"fail");
//                        getMvpView().hideLoading();
//                    }
//                })
                .subscribe(new Consumer<UserResponse.UserDataResponse>() {
                    @Override
                    public void accept(@NonNull UserResponse.UserDataResponse userDataResponse)
                            throws Exception {
                        Log.d(TAG, "accept");
                        if (userDataResponse != null && userDataResponse.getData() != null) {
                            Log.d(TAG, "accept in");
                            getMvpView().setUserInfomation(userDataResponse.getData());
                        }
                        getMvpView().hideLoading();
//                        getCompositeDisposable().dispose();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
