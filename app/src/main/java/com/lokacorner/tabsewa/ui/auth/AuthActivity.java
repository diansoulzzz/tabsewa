package com.lokacorner.tabsewa.ui.auth;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.ui.auth.login.LoginFragment;
import com.lokacorner.tabsewa.ui.auth.register.RegisterFragment;
import com.lokacorner.tabsewa.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AuthActivity extends BaseActivity implements AuthMvpView {

    @Inject
    AuthMvpPresenter<AuthMvpView> mPresenter;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    Integer startTab;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final String TAG = "AuthActivity";

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AuthActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(AuthActivity.this);
        Log.d(TAG, "Started Here");
        setUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_home, menu);
        return true;
    }

    @Override
    protected void setUp() {
//        setSupportActionBar(toolbar);
//        toolbar.inflateMenu(R.menu.toolbar_home);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        startTab = getIntent().getIntExtra("tab", 0);
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.setCurrentItem(startTab);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void openMainActivity() {

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "Items : " + String.valueOf(position));
            switch (position) {
                case 0:
                    return LoginFragment.newInstance();
                case 1:
                    return RegisterFragment.newInstance();
            }
            return RegisterFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "LOGIN";
                case 1:
                    return "REGISTER";
            }
            return null;
        }
    }
}
