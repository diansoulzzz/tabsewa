package com.lokacorner.tabsewa.ui.main.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.data.db.model.Carousel;
import com.lokacorner.tabsewa.di.component.ActivityComponent;
import com.lokacorner.tabsewa.ui.barang_detail.BarangDetailActivity;
import com.lokacorner.tabsewa.ui.base.BaseFragment;
import com.lokacorner.tabsewa.ui.search.SearchActivity;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeSourceFragment extends BaseFragment implements
        HomeSourceMvpView, HomeSourceAdapter.Callback, BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    private static final String TAG = "SearchSourceFragment";

    @Inject
    HomeSourceMvpPresenter<HomeSourceMvpView> mPresenter;

    @Inject
    HomeSourceAdapter.HomeSourceAdapterBarang mHomeSourceAdapterBarang;

    @Inject
    HomeSourceAdapter.HomeSourceAdapterBarangKategori mHomeSourceAdapterBarangKategori;

    @Inject
    LinearLayoutManager mLinearLayoutManager;

    @Inject
    GridLayoutManager mGridLayoutManager;

    @BindView(R.id.category_recycler_view)
    RecyclerView mCategoryView;

    @BindView(R.id.slider_layout)
    SliderLayout mSliderLayout;

    @BindView(R.id.recommend_recycler_view)
    RecyclerView mRecommendView;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

//    @BindView(R.id.refreshLayout)
//    RefreshLayout refreshLayout;

    @BindView(R.id.title_recommend)
    TextView title_recommend;

    @BindView(R.id.title_category)
    TextView title_category;

    @BindView(R.id.btn_recommend_all)
    Button btn_recommend_all;

    @BindView(R.id.child_layout)
    ConstraintLayout child_layout;

    public static HomeSourceFragment newInstance() {
        Bundle args = new Bundle();
        HomeSourceFragment fragment = new HomeSourceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.toolbar_home, menu);
//        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_home, menu);
//        final MenuItem item = menu.findItem(R.id.menu_search);
//        final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
//        searchView.setIconifiedByDefault(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_search:
//                openSearchActivity();
//                break;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            setHasOptionsMenu(true);
            mHomeSourceAdapterBarang.setCallback(this);
            mHomeSourceAdapterBarangKategori.setCallback(this);
        }
        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            swiperefresh.setRefreshing(false);
            mPresenter.onViewPrepared();
        }
    };



    @Override
    protected void setUp(View view) {
        swiperefresh.setOnRefreshListener(onRefreshListener);

        mGridLayoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false);
        mCategoryView.setLayoutManager(mGridLayoutManager);
        mCategoryView.setItemAnimator(new DefaultItemAnimator());

        mGridLayoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
//        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecommendView.setLayoutManager(mGridLayoutManager);
        mRecommendView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();
    }

    @Override
    public void setUpAdapter() {
        mCategoryView.setAdapter(mHomeSourceAdapterBarangKategori);
        mRecommendView.setAdapter(mHomeSourceAdapterBarang);
        mHomeSourceAdapterBarangKategori.clear();
        mHomeSourceAdapterBarang.clear();
    }

//    @Override
//    public void updateLatest(List<Barang> barangList) {
////        mHomeSourceAdapter.addItems(barangList);
//    }

    @Override
    public void addItemAdapterBarang(List<Barang> barangs) {
        mHomeSourceAdapterBarang.addItems(barangs);
    }

    @Override
    public void addItemAdapterBarangKategori(List<BarangKategori> barangKategoris) {
        mHomeSourceAdapterBarangKategori.addItems(barangKategoris);
    }

    @Override
    public void showHeaderList(Integer visibility) {
        child_layout.setVisibility(visibility);
//        title_category.setVisibility(visibility);
//        title_recommend.setVisibility(visibility);
//        btn_recommend_all.setVisibility(visibility);
    }

    @Override
    public void openBarangDetailActivity(Barang barang) {
        Intent intent = BarangDetailActivity.getStartIntent(this.getContext());
        intent.putExtra("barang_id", barang.getId());
        startActivity(intent);
    }

    @Override
    public void setUpSlider(List<Carousel> carouselList) {
        mSliderLayout.removeAllSliders();
        for (int i = 0; i < carouselList.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            sliderView
                    .image(carouselList.get(i).getFotoUrl())
                    .description(carouselList.get(i).getNama())
                    .setRequestOption(new RequestOptions().centerCrop())
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", carouselList.get(i).getNama());
            mSliderLayout.addSlider(sliderView);
        }
        mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSliderLayout.setCustomAnimation(new DescriptionAnimation());
        mSliderLayout.setDuration(4000);
        mSliderLayout.addOnPageChangeListener(this);
    }

    @Override
    public void onDestroyView() {
        mSliderLayout.stopAutoCycle();
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onBarangEmptyViewRetryClick() {
        mPresenter.onViewPrepared();
    }

    @Override
    public void onBarangDetailClick(Barang barang) {
//        Toast.makeText(getContext(), barang.getNama(),Toast.LENGTH_LONG).show();
        openBarangDetailActivity(barang);
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
        Toast.makeText(getContext(), baseSliderView.getBundle().get("extra").toString() + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
