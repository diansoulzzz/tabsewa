package com.lokacorner.tabsewa.ui.main.kategori;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.data.db.model.BarangSubKategori;
import com.lokacorner.tabsewa.di.component.ActivityComponent;
import com.lokacorner.tabsewa.ui.base.BaseFragment;
import com.lokacorner.tabsewa.ui.main.MainMvpView;
import com.lokacorner.tabsewa.utils.CrossfadeWrapper;
import com.lokacorner.tabsewa.utils.CustomMiniDrawer;
import com.lokacorner.tabsewa.utils.CustomUrlPrimaryDrawerItem;
import com.mikepenz.crossfader.Crossfader;
import com.mikepenz.crossfader.util.UIUtils;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KategoriSourceFragment extends BaseFragment implements
        KategoriSourceMvpView, Drawer.OnDrawerItemClickListener {

    private static final String TAG = "SearchSourceFragment";

    @Inject
    KategoriSourceMvpPresenter<KategoriSourceMvpView> mPresenter;

    private Drawer result = null;
    private MiniDrawer miniResult = null;
    private Crossfader crossFader;
    @BindView(R.id.crossfade_content)
    FrameLayout crossfade_content;
    @BindView(R.id.list_category)
    ListView list_category;

    Bundle savedInstanceState;
    private List<BarangKategori> barangKategoriList;
//    String[] list_item_menu = {"Orientation", "Orientation1", "Orientation2", "Orientation3", "Orientation4", "Orientation5", "Orientation6", "Orientation7", "Nombre de Chambres", "Nombre de Salle de bains", "Nombre de toilettes", "Cave", "Parking", "Garage", "Jardin"};


    private MainMvpView listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MainMvpView) context;
        } catch (ClassCastException castException) {
            /* The activity does not implement the listener. */
        }
    }

    public static KategoriSourceFragment newInstance() {
        Bundle args = new Bundle();
        KategoriSourceFragment fragment = new KategoriSourceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            setHasOptionsMenu(true);
            this.savedInstanceState = savedInstanceState;
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.onViewPrepared();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }


//    Drawer.OnDrawerItemClickListener drawerItemClickListener = new Drawer.OnDrawerItemClickListener() {
//        @Override
//        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//            if (drawerItem instanceof Nameable) {
////                Toast.makeText(getBaseActivity(), ((Nameable) drawerItem).getName().getText(getBaseActivity()), Toast.LENGTH_SHORT).show();
//                ArrayAdapter adapter = new ArrayAdapter<BarangSubKategori>(getBaseActivity(), R.layout.item_barang_kategori, barangKategoriList.get(position).getBarangSubKategoris());
//                list_category.setAdapter(adapter);
//            }
//            try {
//                return miniResult.onItemClick(drawerItem);
//            } catch (Exception ignored) {
//
//            }
//            return false;
//        }
//    };


    @Override
    public void setUpDrawer(List<BarangKategori> barangKategoriList) {
        this.barangKategoriList = barangKategoriList;
//        PrimaryDrawerItem[] drawerItems = {
//                new PrimaryDrawerItem().withName(R.string.menu_home).withIcon(GoogleMaterial.Icon.gmd_wb_sunny).withIdentifier(1),
//                new PrimaryDrawerItem().withName(R.string.menu_category).withIcon(FontAwesome.Icon.faw_gamepad).withIdentifier(3)
//        };
        List<CustomUrlPrimaryDrawerItem> drawerKategori = new ArrayList<CustomUrlPrimaryDrawerItem>();
        int i = 0;
        for (BarangKategori barangKategori : barangKategoriList) {
            drawerKategori.add(new CustomUrlPrimaryDrawerItem().withName(barangKategori.getNama()).withIcon(barangKategori.getFotoUrl()).withIdentifier(i));
            i++;
        }

//        result = new DrawerBuilder()
//                .withActivity(getBaseActivity())
//                .withTranslucentNavigationBar(false)
//                .addDrawerItems(drawerKategori.toArray(new PrimaryDrawerItem[drawerKategori.size()]))
//                .withOnDrawerItemClickListener(drawerItemClickListener)
//                .withFireOnInitialOnClick(true)
////                .withGenerateMiniDrawer(true)
//                .buildView();

        DrawerBuilder builder = new DrawerBuilder()
                .withActivity(getBaseActivity())
                .withTranslucentStatusBar(false)
                .addDrawerItems(drawerKategori.toArray(new CustomUrlPrimaryDrawerItem[drawerKategori.size()]))
//                .withOnDrawerItemClickListener(drawerItemClickListener)
                .withOnDrawerItemClickListener(this)
                .withFireOnInitialOnClick(true)
                .withSavedInstance(this.savedInstanceState);

        result = builder.buildView();
        miniResult = new CustomMiniDrawer().withDrawer(result).withEnableSelectedMiniDrawerItemBackground(true);
//        miniResult = new CustomMiniDrawer().withDrawer(result);

//        miniResult = result.getMiniDrawer();
        int firstWidth = (int) UIUtils.convertDpToPixel(200, getBaseActivity());
        int secondWidth = (int) UIUtils.convertDpToPixel(100, getBaseActivity());

        crossFader = new Crossfader()
                .withContent(crossfade_content)
                .withFirst(result.getSlider(), firstWidth)
                .withSecond(miniResult.build(getBaseActivity()), secondWidth)
                .build();

        miniResult.withCrossFader(new CrossfadeWrapper(crossFader));
        crossFader.getCrossFadeSlidingPaneLayout().setShadowResourceLeft(R.drawable.material_drawer_shadow_left);
    }

    @Override
    public void setUpSubKategori() {
        list_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        if (drawerItem instanceof Nameable) {
            ArrayAdapter adapter = new ArrayAdapter<BarangSubKategori>(getBaseActivity(), R.layout.item_barang_kategori, barangKategoriList.get(position).getBarangSubKategoris());
            list_category.setAdapter(adapter);
        }
        try {
            return miniResult.onItemClick(drawerItem);
        } catch (Exception ignored) {

        }
        return false;
    }
}
