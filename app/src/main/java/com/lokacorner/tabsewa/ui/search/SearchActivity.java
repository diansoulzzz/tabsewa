package com.lokacorner.tabsewa.ui.search;


import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.ui.base.BaseActivity;
import com.lokacorner.tabsewa.ui.search_filter.SearchResultActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchActivity extends BaseActivity implements SearchMvpView, SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search_view)
    SearchView search_view;
    @Inject
    SearchMvpPresenter<SearchMvpView> mPresenter;
    private static final String TAG = "SearchResultActivity";
    private RecyclerView mRecyclerView;
    //    private SearchResultAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    //    private SearchView mSearchView;
    private String mQuery;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SearchActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(SearchActivity.this);
        Log.d(TAG, "Started Here");
        setUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.toolbar_search, menu);
//        MenuItem searchItem = menu.findItem(R.id.menu_search);
//        mSearchView = (SearchView) searchItem.getActionView();
//        setupSearchView(searchItem);
//        if (mQuery != null) {
//            mSearchView.setQuery(mQuery, false);
//        }
//        return true;
//    }

    @Override
    public void setupSearchView() {
        search_view.setIconifiedByDefault(false);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();
            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null
                        && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            search_view.setSearchableInfo(info);
        }
        search_view.setOnQueryTextListener(this);
        search_view.setFocusable(false);
        search_view.setFocusableInTouchMode(false);
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            mQuery = null;
        } else {
            mQuery = extras.getString("query");
        }
        setupSearchView();
        if (mQuery != null) {
            search_view.setQuery(mQuery, false);
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
//        Intent intent = new Intent(Activity.this, SearchResultActivity.class);
//        intent.putExtra("query", query);
//        startActivity(intent);
        Intent intent = SearchResultActivity.getStartIntent(this);
        intent.putExtra("query", query);
        startActivity(intent);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
