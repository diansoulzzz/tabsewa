package com.lokacorner.tabsewa.ui.register_vendor;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class RegisterVendorActivity extends BaseActivity implements RegisterVendorMvpView {

    @Inject
    RegisterVendorMvpPresenter<RegisterVendorMvpView> mPresenter;
    private static final String TAG = "RegisterVendorActivity";

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, RegisterVendorActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(RegisterVendorActivity.this);
        Log.d(TAG, "Started Here");
        setUp();
    }

    @Override
    protected void setUp() {

    }

    @Override
    public void openMainActivity() {

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

}
