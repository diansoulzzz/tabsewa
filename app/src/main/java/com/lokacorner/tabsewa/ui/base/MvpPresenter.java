package com.lokacorner.tabsewa.ui.base;

import com.androidnetworking.error.ANError;

/**
 * Created by GBS on 31/01/2018.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(ANError error);

    void setUserAsLoggedOut();
}
