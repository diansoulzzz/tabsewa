package com.lokacorner.tabsewa.ui.auth;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface AuthMvpPresenter<V extends AuthMvpView> extends MvpPresenter<V> {

    void onViewInitialized();

}
