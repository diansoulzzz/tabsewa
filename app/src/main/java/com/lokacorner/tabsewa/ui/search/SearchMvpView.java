package com.lokacorner.tabsewa.ui.search;

import android.view.MenuItem;

import com.lokacorner.tabsewa.ui.base.MvpView;

public interface SearchMvpView extends MvpView {

    void setupSearchView();
}
