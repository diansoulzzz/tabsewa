package com.lokacorner.tabsewa.ui.empty;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.ui.base.BaseActivity;
import com.lokacorner.tabsewa.ui.main.account.AccountSourceFragment;
import com.lokacorner.tabsewa.ui.main.home.HomeSourceFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EmptyActivity extends BaseActivity implements EmptyMvpView {

    @Inject
    EmptyMvpPresenter<EmptyMvpView> mPresenter;
    private static final String TAG = "EmptyActivity";

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, EmptyActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(EmptyActivity.this);
        Log.d(TAG, "Started Here");
        setUp();
    }

    @Override
    protected void setUp() {

    }

    @Override
    public void openMainActivity() {

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

}
