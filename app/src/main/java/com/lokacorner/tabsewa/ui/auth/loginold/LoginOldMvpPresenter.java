package com.lokacorner.tabsewa.ui.auth.loginold;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.base.MvpPresenter;

@PerActivity
public interface LoginOldMvpPresenter<V extends LoginOldMvpView> extends MvpPresenter<V> {

    void onServerLoginClick(String email, String password);

    void onGoogleRequestToken(String uid,String token);

    void onFacebookRequestToken(String uid,String token);

    void onFacebookAuth(AccessToken accessToken);

    void onGoogleAuth(GoogleSignInAccount googleSignInAccount);

}
