package com.lokacorner.tabsewa.ui.main.home;


import android.view.View;

import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.data.db.model.Carousel;
import com.lokacorner.tabsewa.ui.base.MvpView;
import java.util.List;

public interface HomeSourceMvpView extends MvpView {

    void setUpAdapter();

//    void updateLatest(List<Barang> barangList);

    void setUpSlider(List<Carousel> carouselList);

    void addItemAdapterBarangKategori(List<BarangKategori> objects);

    void addItemAdapterBarang(List<Barang> objects);

    void showHeaderList(Integer visibility);

    void openBarangDetailActivity(Barang barang);
}
