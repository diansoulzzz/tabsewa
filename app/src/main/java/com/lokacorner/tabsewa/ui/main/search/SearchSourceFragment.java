package com.lokacorner.tabsewa.ui.main.search;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.di.component.ActivityComponent;
import com.lokacorner.tabsewa.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchSourceFragment extends BaseFragment implements
        SearchSourceMvpView, SearchSourceAdapter.Callback, BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    private static final String TAG = "SearchSourceFragment";

    @Inject
    SearchSourceMvpPresenter<SearchSourceMvpView> mPresenter;

    @Inject
    SearchSourceAdapter.HomeSourceAdapterBarang mHomeSourceAdapterBarang;

    @Inject
    SearchSourceAdapter.HomeSourceAdapterBarangKategori mHomeSourceAdapterBarangKategori;

    @Inject
    LinearLayoutManager mLinearLayoutManager;

    @Inject
    GridLayoutManager mGridLayoutManager;

    @BindView(R.id.category_recycler_view)
    RecyclerView mCategoryView;

    @BindView(R.id.slider_layout)
    SliderLayout mSliderLayout;

    @BindView(R.id.recommend_recycler_view)
    RecyclerView mRecommendView;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    @BindView(R.id.title_recommend)
    TextView title_recommend;

    @BindView(R.id.title_category)
    TextView title_category;

    @BindView(R.id.btn_recommend_all)
    Button btn_recommend_all;

    public static SearchSourceFragment newInstance() {
        Bundle args = new Bundle();
        SearchSourceFragment fragment = new SearchSourceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mHomeSourceAdapterBarang.setCallback(this);
            mHomeSourceAdapterBarangKategori.setCallback(this);
            swiperefresh.setOnRefreshListener(onRefreshListener);
        }
        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            swiperefresh.setRefreshing(false);
            mPresenter.onViewPrepared();
        }
    };

    @Override
    protected void setUp(View view) {
        mGridLayoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false);
        mCategoryView.setLayoutManager(mGridLayoutManager);
        mCategoryView.setItemAnimator(new DefaultItemAnimator());

        mGridLayoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
//        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecommendView.setLayoutManager(mGridLayoutManager);
        mRecommendView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();
    }

    @Override
    public void setUpAdapter() {
        mCategoryView.setAdapter(mHomeSourceAdapterBarangKategori);
        mRecommendView.setAdapter(mHomeSourceAdapterBarang);
        mHomeSourceAdapterBarangKategori.clear();
        mHomeSourceAdapterBarang.clear();
    }

    @Override
    public void updateLatest(List<Barang> barangList) {
//        mHomeSourceAdapter.addItems(barangList);
    }

    @Override
    public void addItemAdapterBarang(List<Barang> barangs) {
        mHomeSourceAdapterBarang.addItems(barangs);
    }
    @Override
    public void addItemAdapterBarangKategori(List<BarangKategori> barangKategoris) {
        mHomeSourceAdapterBarangKategori.addItems(barangKategoris);
    }
    @Override
    public void showHeaderList(Integer visibility){
        title_category.setVisibility(visibility);
        title_recommend.setVisibility(visibility);
        btn_recommend_all.setVisibility(visibility);
    }
    @Override
    public void setUpSlider(List<Barang> barangList) {
        mSliderLayout.removeAllSliders();
        for (int i = 0; i < barangList.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            sliderView
                    .image(barangList.get(i).getBarangFotoUtama().getFotoUrl())
                    .description(barangList.get(i).getNama())
                    .setRequestOption(new RequestOptions().centerCrop())
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", barangList.get(i).getNama());
            mSliderLayout.addSlider(sliderView);
        }
        mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSliderLayout.setCustomAnimation(new DescriptionAnimation());
        mSliderLayout.setDuration(4000);
        mSliderLayout.addOnPageChangeListener(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onBarangEmptyViewRetryClick() {

    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
        Toast.makeText(getContext(), baseSliderView.getBundle().get("extra").toString() + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
