package com.lokacorner.tabsewa.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DimenRes;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lokacorner.tabsewa.R;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.holder.DimenHolder;
import com.mikepenz.materialdrawer.holder.ImageHolder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.BaseDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialize.util.UIUtils;

import java.util.List;

public class CustomMiniDrawerItem extends BaseDrawerItem<CustomMiniDrawerItem, CustomMiniDrawerItem.ViewHolder> {

    private StringHolder mBadge;
    private BadgeStyle mBadgeStyle = new BadgeStyle();
    private StringHolder mTitleItem;


    private boolean mEnableSelectedBackground = false;
    protected DimenHolder mCustomHeight;

    public CustomMiniDrawerItem() {

    }

    public CustomMiniDrawerItem(PrimaryDrawerItem primaryDrawerItem) {
        this.mIdentifier = primaryDrawerItem.getIdentifier();

        this.mTag = primaryDrawerItem.getTag();
        this.mTitleItem = primaryDrawerItem.getName();
        this.mBadge = primaryDrawerItem.getBadge(); // i didn't understood, how to connect this
        this.mBadgeStyle = primaryDrawerItem.getBadgeStyle();// i didn't understood, how to connect this

        this.mEnabled = primaryDrawerItem.isEnabled();
        this.mSelectable = primaryDrawerItem.isSelectable();
        this.mSelected = primaryDrawerItem.isSelected();

        this.icon = primaryDrawerItem.getIcon();
        this.selectedIcon = primaryDrawerItem.getSelectedIcon();

        this.iconTinted = primaryDrawerItem.isIconTinted();
        this.selectedColor = primaryDrawerItem.getSelectedColor();

        this.iconColor = primaryDrawerItem.getIconColor();
        this.selectedIconColor = primaryDrawerItem.getSelectedIconColor();
        this.disabledIconColor = primaryDrawerItem.getDisabledIconColor();
    }

    public CustomMiniDrawerItem(CustomUrlPrimaryDrawerItem primaryDrawerItem) {
        this.mIdentifier = primaryDrawerItem.getIdentifier();

        this.mTag = primaryDrawerItem.getTag();
        this.mTitleItem = primaryDrawerItem.getName();
        this.mBadge = primaryDrawerItem.getBadge(); // i didn't understood, how to connect this
        this.mBadgeStyle = primaryDrawerItem.getBadgeStyle();// i didn't understood, how to connect this

        this.mEnabled = primaryDrawerItem.isEnabled();
        this.mSelectable = primaryDrawerItem.isSelectable();
        this.mSelected = primaryDrawerItem.isSelected();

        this.icon = primaryDrawerItem.getIcon();
        this.selectedIcon = primaryDrawerItem.getSelectedIcon();

        this.iconTinted = primaryDrawerItem.isIconTinted();
        this.selectedColor = primaryDrawerItem.getSelectedColor();

        this.iconColor = primaryDrawerItem.getIconColor();
        this.selectedIconColor = primaryDrawerItem.getSelectedIconColor();
        this.disabledIconColor = primaryDrawerItem.getDisabledIconColor();
    }

    public CustomMiniDrawerItem(SecondaryDrawerItem secondaryDrawerItem) {
        this.mIdentifier = secondaryDrawerItem.getIdentifier();
        this.mTag = secondaryDrawerItem.getTag();
        this.mTitleItem = secondaryDrawerItem.getName();
        this.mBadge = secondaryDrawerItem.getBadge(); // i didn't understood, how to connect this
        this.mBadgeStyle = secondaryDrawerItem.getBadgeStyle();// i didn't understood, how to connect this

        this.mEnabled = secondaryDrawerItem.isEnabled();
        this.mSelectable = secondaryDrawerItem.isSelectable();
        this.mSelected = secondaryDrawerItem.isSelected();

        this.icon = secondaryDrawerItem.getIcon();
        this.selectedIcon = secondaryDrawerItem.getSelectedIcon();

        this.iconTinted = secondaryDrawerItem.isIconTinted();
        this.selectedColor = secondaryDrawerItem.getSelectedColor();

        this.iconColor = secondaryDrawerItem.getIconColor();
        this.selectedIconColor = secondaryDrawerItem.getSelectedIconColor();
        this.disabledIconColor = secondaryDrawerItem.getDisabledIconColor();
    }


    public CustomMiniDrawerItem withCustomHeightRes(@DimenRes int customHeightRes) {
        this.mCustomHeight = DimenHolder.fromResource(customHeightRes);
        return this;
    }

    public CustomMiniDrawerItem withCustomHeightDp(int customHeightDp) {
        this.mCustomHeight = DimenHolder.fromDp(customHeightDp);
        return this;
    }

    public CustomMiniDrawerItem withCustomHeightPx(int customHeightPx) {
        this.mCustomHeight = DimenHolder.fromPixel(customHeightPx);
        return this;
    }

    public CustomMiniDrawerItem withCustomHeight(DimenHolder customHeight) {
        this.mCustomHeight = customHeight;
        return this;
    }

    public CustomMiniDrawerItem withEnableSelectedBackground(boolean enableSelectedBackground) {
        this.mEnableSelectedBackground = enableSelectedBackground;
        return this;
    }

    @Override
    public int getType() {
        return R.id.material_drawer_item_mini;
    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.custom_material_drawer_item_mini;
    }


    @Override
    public void bindView(ViewHolder viewHolder, List payloads) {
        super.bindView(viewHolder, payloads);

        Context ctx = viewHolder.itemView.getContext();

        //set a different height for this item
        if (mCustomHeight != null) {
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) viewHolder.itemView.getLayoutParams();
            lp.height = mCustomHeight.asPixel(ctx);
            viewHolder.itemView.setLayoutParams(lp);
        }

        //set the identifier from the drawerItem here. It can be used to run tests
        viewHolder.itemView.setId(hashCode());

        //set the item enabled if it is
        viewHolder.itemView.setEnabled(isEnabled());

        //set the item selected if it is
        viewHolder.itemView.setSelected(isSelected());
        viewHolder.titleMiniDrawer.setText(mTitleItem.getText());
        //
        viewHolder.itemView.setTag(this);

        int color = getColor(ctx);
        int selectedTextColor = getSelectedTextColor(ctx);

        //get the correct color for the icon
        int iconColor = getIconColor(ctx);
        int selectedIconColor = getSelectedIconColor(ctx);

        if (mEnableSelectedBackground) {
            //get the correct color for the background
            int selectedColor = getSelectedColor(ctx);
            //set the background for the item
//            UIUtils.setBackground(viewHolder.view, UIUtils.getSelectableBackground(ctx,selectedColor, isSelectedBackgroundAnimated()));
            UIUtils.setBackground(viewHolder.view, UIUtils.getSelectableBackground(ctx, R.color.gray, true));
        }

        viewHolder.titleMiniDrawer.setTextColor(getTextColorStateList(color, selectedTextColor));

        //set the text for the badge or hide
        boolean badgeVisible = StringHolder.applyToOrHide(mBadge, viewHolder.badge);
        //style the badge if it is visible
        if (badgeVisible) {
            mBadgeStyle.style(viewHolder.badge);
        }

        //get the drawables for our icon and set it
//        Drawable icon = ImageHolder.decideIcon(getIcon(), ctx, iconColor, isIconTinted(), 1);
//        Drawable selectedIcon = ImageHolder.decideIcon(getSelectedIcon(), ctx, selectedIconColor, isIconTinted(), 1);
//        ImageHolder.applyMultiIconTo(icon, iconColor, selectedIcon, selectedIconColor, isIconTinted(), viewHolder.icon);

        DrawerImageLoader.getInstance().cancelImage(viewHolder.icon);
        viewHolder.icon.setImageBitmap(null);
        //get the drawables for our icon and set it
        ImageHolder.applyTo(icon, viewHolder.icon, "customUrlItem");

        //for android API 17 --> Padding not applied via xml
        int verticalPadding = ctx.getResources().getDimensionPixelSize(R.dimen.material_drawer_padding);
        int topBottomPadding = ctx.getResources().getDimensionPixelSize(R.dimen.material_mini_drawer_item_padding);
        viewHolder.itemView.setPadding(verticalPadding, topBottomPadding, verticalPadding, topBottomPadding);

        //call the onPostBindView method to trigger post bind view actions (like the listener to modify the item if required)
        onPostBindView(this, viewHolder.itemView);
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private ImageView icon;
        private TextView badge;
        private TextView titleMiniDrawer;


        public ViewHolder(View view) {
            super(view);
            this.view = view;
            this.icon = (ImageView) view.findViewById(R.id.material_drawer_icon);
            this.badge = (TextView) view.findViewById(R.id.material_drawer_badge);
            this.titleMiniDrawer = (TextView) view.findViewById(R.id.material_drawer_title);
        }
    }
}