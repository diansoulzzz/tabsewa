package com.lokacorner.tabsewa.utils;

import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import static com.mikepenz.materialdrawer.MiniDrawer.ITEM;
import static com.mikepenz.materialdrawer.MiniDrawer.PROFILE;

public class CustomMiniDrawer extends MiniDrawer {
    private boolean mEnableProfileClick = true;
    private boolean mEnableSelectedMiniDrawerItemBackground = false;

    @Override
    public int getMiniDrawerType(IDrawerItem drawerItem) {
        if (drawerItem instanceof MiniProfileDrawerItem) {
            return PROFILE;
        } else if (drawerItem instanceof CustomMiniDrawerItem) {
            return ITEM;
        }
        return -1;
    }

    private boolean mIncludeSecondaryDrawerItems = false;
    public CustomMiniDrawer withIncludeSecondaryDrawerItems(boolean includeSecondaryDrawerItems) {
        this.mIncludeSecondaryDrawerItems = includeSecondaryDrawerItems;
        return this;
    }

    @Override
    public IDrawerItem generateMiniDrawerItem(IDrawerItem drawerItem) {
        if (drawerItem instanceof SecondaryDrawerItem) {
            return mIncludeSecondaryDrawerItems ? new CustomMiniDrawerItem((SecondaryDrawerItem) drawerItem).withEnableSelectedBackground(mEnableSelectedMiniDrawerItemBackground) : null;
        } else if (drawerItem instanceof PrimaryDrawerItem) {
            return new CustomMiniDrawerItem((PrimaryDrawerItem) drawerItem).withEnableSelectedBackground(mEnableSelectedMiniDrawerItemBackground);
        } else if (drawerItem instanceof CustomUrlPrimaryDrawerItem) {
            return new CustomMiniDrawerItem((CustomUrlPrimaryDrawerItem) drawerItem).withEnableSelectedBackground(mEnableSelectedMiniDrawerItemBackground);
//        } else if (drawerItem instanceof CustomPrimaryDrawerItem) {
//            return new CustomMiniDrawerItem((CustomPrimaryDrawerItem) drawerItem).withEnableSelectedBackground(mEnableSelectedMiniDrawerItemBackground);
        }else if (drawerItem instanceof ProfileDrawerItem) {
            MiniProfileDrawerItem mpdi = new MiniProfileDrawerItem((ProfileDrawerItem) drawerItem);
            mpdi.withEnabled(mEnableProfileClick);
            return mpdi;
        }
        return null;
    }
}
