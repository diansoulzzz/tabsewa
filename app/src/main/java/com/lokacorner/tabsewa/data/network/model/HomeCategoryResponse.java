package com.lokacorner.tabsewa.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.data.db.model.BarangSubKategori;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GBS on 07/02/2018.
 */

public class HomeCategoryResponse {

    private HomeCategoryResponse() {
        // This class is not publicly instantiable
    }

    public static class HomeCategoryDataResponse {
        @SerializedName("data")
        @Expose
        private Data data = new Data();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public Data getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(Data data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }

    public static class Data {
        @SerializedName("barang")
        @Expose
        private List<Barang> barang = new ArrayList<Barang>();
        @SerializedName("kategori")
        @Expose
        private List<BarangKategori> kategori = new ArrayList<BarangKategori>();

        public List<Barang> getBarangList() {
            return barang;
        }

        public void setBarangList(List<Barang> barang) {
            this.barang = barang;
        }

        public List<BarangKategori> getBarangKategoriList() {
            return kategori;
        }

        public void setBarangKategoriList(List<BarangKategori> kategori) {
            this.kategori = kategori;
        }

    }
}
