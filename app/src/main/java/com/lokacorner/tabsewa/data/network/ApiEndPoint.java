package com.lokacorner.tabsewa.data.network;

import com.lokacorner.tabsewa.BuildConfig;

public final class ApiEndPoint {

    public static final String ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL
            + "login/google";

    public static final String ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL
            + "login/facebook";

    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL
            + "login/server";

    public static final String ENDPOINT_LOGOUT = BuildConfig.BASE_URL
            + "logout";

    public static final String ENDPOINT_BLOG = BuildConfig.BASE_URL
            + "5926ce9d11000096006ccb30";

    public static final String ENDPOINT_USER_GOOGLE = BuildConfig.BASE_URL
            + "user";

    public static final String ENDPOINT_OPEN_SOURCE = BuildConfig.BASE_URL
            + "5926c34212000035026871cd";

    public static final String ENDPOINT_USER = BuildConfig.BASE_URL
            + "user";

    public static final String ENDPOINT_USER_PROFILE = BuildConfig.BASE_URL
            + "profile/info";

    public static final String ENDPOINT_BARANG_DATA = BuildConfig.BASE_URL
            + "item/data";

    public static final String ENDPOINT_BARANG_LIST = BuildConfig.BASE_URL
            + "item/list";

    public static final String ENDPOINT_CAROUSEL = BuildConfig.BASE_URL
            + "carousel";

    public static final String ENDPOINT_CATEGORY = BuildConfig.BASE_URL
            + "category";

    public static final String ENDPOINT_CATEGORY_DATA = BuildConfig.BASE_URL
            + "category/data";

    public static final String ENDPOINT_CATEGORY_LIST = BuildConfig.BASE_URL
            + "category/list";


    public static final String ENDPOINT_ITEM_LATEST = BuildConfig.BASE_URL
            + "item/latest";

    public static final String ENDPOINT_HOME_DATA = BuildConfig.BASE_URL
            + "home/data";

//    public static final String ENDPOINT_ITEM_DATA = BuildConfig.BASE_URL
//            + "/api/item/data";



    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
