package com.lokacorner.tabsewa.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GBS on 07/02/2018.
 */

public class CategoryResponse {

    private CategoryResponse() {
        // This class is not publicly instantiable
    }

    public static class CategoryDataResponse {
        @SerializedName("data")
        @Expose
        private BarangKategori data = new BarangKategori();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public BarangKategori getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(BarangKategori data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }

    public static class CategoryListResponse {
        @SerializedName("data")
        @Expose
        private List<BarangKategori> data = new ArrayList<BarangKategori>();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public List<BarangKategori> getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(List<BarangKategori> data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }
}
