package com.lokacorner.tabsewa.data.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lokacorner.tabsewa.di.AcceptHeader;
import com.lokacorner.tabsewa.di.ApiInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ApiHeader {

    private ProtectedApiHeader mProtectedApiHeader;
    private PublicApiHeader mPublicApiHeader;

    @Inject
    public ApiHeader(PublicApiHeader publicApiHeader, ProtectedApiHeader protectedApiHeader) {
        mPublicApiHeader = publicApiHeader;
        mProtectedApiHeader = protectedApiHeader;
    }

    public ProtectedApiHeader getProtectedApiHeader() {
        return mProtectedApiHeader;
    }

    public PublicApiHeader getPublicApiHeader() {
        return mPublicApiHeader;
    }

    public static final class PublicApiHeader {

        @Expose
        @SerializedName("Accept")
        private String mAccept;

        @Inject
        public PublicApiHeader(@AcceptHeader String mAccept) {
            mAccept = mAccept;
        }

        public String getmAccept() {
            return mAccept;
        }

        public void setmAccept(String mAccept) {
            this.mAccept = mAccept;
        }
    }

    public static final class ProtectedApiHeader {

        @Expose
        @SerializedName("Accept")
        private String mAccept;

        @Expose
        @SerializedName("Authorization")
        private String mAuthorization;

        public ProtectedApiHeader(String mAccept, String mAuthorization) {
            this.mAccept = mAccept;
            this.mAuthorization = mAuthorization;
        }

        public String getmAccept() {
            return mAccept;
        }

        public void setmAccept(String mAccept) {
            this.mAccept = mAccept;
        }

        public String getmAuthorization() {
            return mAuthorization;
        }

        public void setmAuthorization(String mAuthorization) {
            this.mAuthorization = mAuthorization;
        }
    }
}
