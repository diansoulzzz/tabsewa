package com.lokacorner.tabsewa.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangRequest {
    private BarangRequest() {
        // This class is not publicly instantiable
    }

    public static class BarangDataRequest {
        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public BarangDataRequest(Integer id) {

            this.id = id;
        }
    }

}
