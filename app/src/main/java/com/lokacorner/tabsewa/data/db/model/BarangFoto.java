package com.lokacorner.tabsewa.data.db.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangFoto {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("foto_url")
    @Expose
    private String fotoUrl;
    @SerializedName("utama")
    @Expose
    private Integer utama;
    @SerializedName("barang_id")
    @Expose
    private Integer barangId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public Integer getUtama() {
        return utama;
    }

    public void setUtama(Integer utama) {
        this.utama = utama;
    }

    public Integer getBarangId() {
        return barangId;
    }

    public void setBarangId(Integer barangId) {
        this.barangId = barangId;
    }

}