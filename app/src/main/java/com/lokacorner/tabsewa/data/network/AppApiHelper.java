package com.lokacorner.tabsewa.data.network;

import com.lokacorner.tabsewa.data.network.model.BarangRequest;
import com.lokacorner.tabsewa.data.network.model.BarangResponse;
import com.lokacorner.tabsewa.data.network.model.BlogResponse;
import com.lokacorner.tabsewa.data.network.model.CategoryResponse;
import com.lokacorner.tabsewa.data.network.model.HomeCategoryResponse;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.data.network.model.LoginRequest;
import com.lokacorner.tabsewa.data.network.model.LoginResponse;
import com.lokacorner.tabsewa.data.network.model.LogoutResponse;
import com.lokacorner.tabsewa.data.network.model.OpenSourceResponse;
import com.lokacorner.tabsewa.data.network.model.UserResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

@Singleton
public class AppApiHelper implements ApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    @Override
    public Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest
                                                              request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_GOOGLE_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                              request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LogoutResponse> doLogoutApiCall() {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_LOGOUT)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(LogoutResponse.class);
    }

    @Override
    public Single<BlogResponse> getBlogApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_BLOG)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(BlogResponse.class);
    }

    @Override
    public Single<OpenSourceResponse> getOpenSourceApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_OPEN_SOURCE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(OpenSourceResponse.class);
    }

    @Override
    public Single<BarangResponse.BarangListResponse> getBarangListResponse() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_BARANG_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(BarangResponse.BarangListResponse.class);
    }

    @Override
    public Single<BarangResponse.BarangDataResponse> getBarangDataResponse(BarangRequest.BarangDataRequest request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_BARANG_DATA)
                .addQueryParameter(request)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(BarangResponse.BarangDataResponse.class);
    }

    @Override
    public Single<HomeResponse.HomeDataResponse> getHomeDataResponse() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_HOME_DATA)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(HomeResponse.HomeDataResponse.class);
    }

    @Override
    public Single<CategoryResponse.CategoryDataResponse> getCategoryDataResponse() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_CATEGORY_DATA)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(CategoryResponse.CategoryDataResponse.class);
    }

    @Override
    public Single<HomeCategoryResponse.HomeCategoryDataResponse> getHomeCategoryDataResponse() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_CATEGORY_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(HomeCategoryResponse.HomeCategoryDataResponse.class);
    }

    @Override
    public Single<CategoryResponse.CategoryListResponse> getCategoryListResponse() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_CATEGORY_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(CategoryResponse.CategoryListResponse.class);
    }

    @Override
    public Single<UserResponse.UserDataResponse> getUserDataResponse() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_USER_PROFILE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(UserResponse.UserDataResponse.class);
    }
//    @Override
//    public Single<BarangResponse.BarangDetailResponse> getBarangDetailResponse() {
//        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_HOME_DATA)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .build()
//                .getObjectSingle(BarangResponse.BarangDetailResponse.class);
//    }

//    @Override
//    public Single<UserDataResponse> getUserDataCall() {
//        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_USER)
//                .addHeaders(mApiHeader.getProtectedApiHeader())
//                .build()
//                .getObjectSingle(UserDataResponse.class);
//    }
}

