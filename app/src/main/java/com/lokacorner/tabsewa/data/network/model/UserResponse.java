package com.lokacorner.tabsewa.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lokacorner.tabsewa.data.db.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GBS on 07/02/2018.
 */

public class UserResponse {

    private UserResponse() {
        // This class is not publicly instantiable
    }

    public static class UserDataResponse {
        @SerializedName("data")
        @Expose
        private User data = new User();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public User getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(User data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }

    public static class UserListResponse {
        @SerializedName("data")
        @Expose
        private List<User> data = new ArrayList<User>();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public List<User> getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(List<User> data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }
}
