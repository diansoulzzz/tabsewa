package com.lokacorner.tabsewa.data.db;


import com.lokacorner.tabsewa.data.db.model.DaoMaster;
import com.lokacorner.tabsewa.data.db.model.DaoSession;
import com.lokacorner.tabsewa.data.db.model.OptionOld;
import com.lokacorner.tabsewa.data.db.model.QuestionOld;
import com.lokacorner.tabsewa.data.db.model.UserOld;

import java.util.List;
import java.util.concurrent.Callable;
import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppDbHelper implements DbHelper {

    private final DaoSession mDaoSession;

    @Inject
    public AppDbHelper(DbOpenHelper dbOpenHelper) {
        mDaoSession = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
    }

    @Override
    public Observable<Long> insertUser(final UserOld user) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return mDaoSession.getUserOldDao().insert(user);
            }
        });
    }

    @Override
    public Observable<List<UserOld>> getAllUsers() {
        return Observable.fromCallable(new Callable<List<UserOld>>() {
            @Override
            public List<UserOld> call() throws Exception {
                return mDaoSession.getUserOldDao().loadAll();
            }
        });
    }

    @Override
    public Observable<List<QuestionOld>> getAllQuestions() {
        return Observable.fromCallable(new Callable<List<QuestionOld>>() {
            @Override
            public List<QuestionOld> call() throws Exception {
                return mDaoSession.getQuestionOldDao().loadAll();
            }
        });
    }

    @Override
    public Observable<Boolean> isQuestionEmpty() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return !(mDaoSession.getQuestionOldDao().count() > 0);
            }
        });
    }

    @Override
    public Observable<Boolean> isOptionEmpty() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return !(mDaoSession.getOptionOldDao().count() > 0);
            }
        });
    }

    @Override
    public Observable<Boolean> saveQuestion(final QuestionOld question) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDaoSession.getQuestionOldDao().insert(question);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> saveOption(final OptionOld option) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDaoSession.getOptionOldDao().insertInTx(option);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> saveQuestionList(final List<QuestionOld> questionList) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDaoSession.getQuestionOldDao().insertInTx(questionList);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> saveOptionList(final List<OptionOld> optionList) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDaoSession.getOptionOldDao().insertInTx(optionList);
                return true;
            }
        });
    }
}
