package com.lokacorner.tabsewa.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lokacorner.tabsewa.data.db.model.Barang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GBS on 07/02/2018.
 */

public class BarangResponse {

    private BarangResponse() {
        // This class is not publicly instantiable
    }

    public static class BarangDataResponse {
        @SerializedName("data")
        @Expose
        private Barang data = new Barang();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public Barang getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(Barang data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }

    public static class BarangListResponse {
        @SerializedName("data")
        @Expose
        private List<Barang> data = new ArrayList<Barang>();
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private int status;

        /**
         * @return The data
         */
        public List<Barang> getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(List<Barang> data) {
            this.data = data;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(int status) {
            this.status = status;
        }

    }
}
