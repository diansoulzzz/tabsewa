package com.lokacorner.tabsewa.data;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Types;
import com.google.gson.reflect.TypeToken;
import com.lokacorner.tabsewa.data.db.DbHelper;
import com.lokacorner.tabsewa.data.db.model.OptionOld;
import com.lokacorner.tabsewa.data.db.model.QuestionOld;
import com.lokacorner.tabsewa.data.db.model.UserOld;
import com.lokacorner.tabsewa.data.network.ApiHeader;
import com.lokacorner.tabsewa.data.network.ApiHelper;
import com.lokacorner.tabsewa.data.network.model.BarangRequest;
import com.lokacorner.tabsewa.data.network.model.BarangResponse;
import com.lokacorner.tabsewa.data.network.model.BlogResponse;
import com.lokacorner.tabsewa.data.network.model.CategoryResponse;
import com.lokacorner.tabsewa.data.network.model.HomeCategoryResponse;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.data.network.model.LoginRequest;
import com.lokacorner.tabsewa.data.network.model.LoginResponse;
import com.lokacorner.tabsewa.data.network.model.LogoutResponse;
import com.lokacorner.tabsewa.data.network.model.OpenSourceResponse;
import com.lokacorner.tabsewa.data.network.model.UserResponse;
import com.lokacorner.tabsewa.data.prefs.PreferencesHelper;
import com.lokacorner.tabsewa.di.ApplicationContext;
import com.lokacorner.tabsewa.utils.AppConstants;
import com.lokacorner.tabsewa.utils.CommonUtils;

import java.lang.reflect.Type;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setmAuthorization(accessToken);
    }

    @Override
    public Observable<Long> insertUser(UserOld user) {
        return mDbHelper.insertUser(user);
    }

    @Override
    public Observable<List<UserOld>> getAllUsers() {
        return mDbHelper.getAllUsers();
    }

    @Override
    public Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest
                                                              request) {
        return mApiHelper.doGoogleLoginApiCall(request);
    }

    @Override
    public Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                request) {
        return mApiHelper.doFacebookLoginApiCall(request);
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                              request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public Single<LogoutResponse> doLogoutApiCall() {
        return mApiHelper.doLogoutApiCall();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public void updateApiHeader(Long userId, String accessToken) {
//        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        mApiHelper.getApiHeader().getProtectedApiHeader().setmAuthorization(accessToken);
    }

    @Override
    public void updateUserInfo(
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath) {

        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserName(userName);
        setCurrentUserEmail(email);
        setCurrentUserProfilePicUrl(profilePicPath);

        updateApiHeader(userId, accessToken);
    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null);
    }

    @Override
    public Observable<Boolean> isQuestionEmpty() {
        return mDbHelper.isQuestionEmpty();
    }

    @Override
    public Observable<Boolean> isOptionEmpty() {
        return mDbHelper.isOptionEmpty();
    }

    @Override
    public Observable<Boolean> saveQuestion(QuestionOld question) {
        return mDbHelper.saveQuestion(question);
    }

    @Override
    public Observable<Boolean> saveOption(OptionOld option) {
        return mDbHelper.saveOption(option);
    }

    @Override
    public Observable<Boolean> saveQuestionList(List<QuestionOld> questionList) {
        return mDbHelper.saveQuestionList(questionList);
    }

    @Override
    public Observable<Boolean> saveOptionList(List<OptionOld> optionList) {
        return mDbHelper.saveOptionList(optionList);
    }

    @Override
    public Observable<List<QuestionOld>> getAllQuestions() {
        return mDbHelper.getAllQuestions();
    }

    @Override
    public Observable<Boolean> seedDatabaseQuestions() {

        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        return mDbHelper.isQuestionEmpty()
                .concatMap(new Function<Boolean, ObservableSource<? extends Boolean>>() {
                    @Override
                    public ObservableSource<? extends Boolean> apply(Boolean isEmpty)
                            throws Exception {
                        if (isEmpty) {
                            Type type = $Gson$Types
                                    .newParameterizedTypeWithOwner(null, List.class,
                                            QuestionOld.class);
                            List<QuestionOld> questionList = gson.fromJson(
                                    CommonUtils.loadJSONFromAsset(mContext,
                                            AppConstants.SEED_DATABASE_QUESTIONS),
                                    type);

                            return saveQuestionList(questionList);
                        }
                        return Observable.just(false);
                    }
                });
    }

    @Override
    public Observable<Boolean> seedDatabaseOptions() {

        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        return mDbHelper.isOptionEmpty()
                .concatMap(new Function<Boolean, ObservableSource<? extends Boolean>>() {
                    @Override
                    public ObservableSource<? extends Boolean> apply(Boolean isEmpty)
                            throws Exception {
                        if (isEmpty) {
                            Type type = new TypeToken<List<OptionOld>>() {
                            }
                                    .getType();
                            List<OptionOld> optionList = gson.fromJson(
                                    CommonUtils.loadJSONFromAsset(mContext,
                                            AppConstants.SEED_DATABASE_OPTIONS),
                                    type);

                            return saveOptionList(optionList);
                        }
                        return Observable.just(false);
                    }
                });
    }

    @Override
    public Single<BlogResponse> getBlogApiCall() {
        return mApiHelper.getBlogApiCall();
    }

    @Override
    public Single<OpenSourceResponse> getOpenSourceApiCall() {
        return mApiHelper.getOpenSourceApiCall();
    }

    @Override
    public Single<BarangResponse.BarangListResponse> getBarangListResponse() {
        return mApiHelper.getBarangListResponse();
    }

    @Override
    public Single<BarangResponse.BarangDataResponse> getBarangDataResponse(BarangRequest.BarangDataRequest request) {
        return mApiHelper.getBarangDataResponse(request);
    }

    @Override
    public Single<HomeResponse.HomeDataResponse> getHomeDataResponse() {
        return mApiHelper.getHomeDataResponse();
    }

    @Override
    public Single<CategoryResponse.CategoryDataResponse> getCategoryDataResponse() {
        return mApiHelper.getCategoryDataResponse();
    }

    @Override
    public Single<HomeCategoryResponse.HomeCategoryDataResponse> getHomeCategoryDataResponse() {
        return mApiHelper.getHomeCategoryDataResponse();
    }

    @Override
    public Single<CategoryResponse.CategoryListResponse> getCategoryListResponse() {
        return mApiHelper.getCategoryListResponse();
    }

    @Override
    public Single<UserResponse.UserDataResponse> getUserDataResponse() {
        return mApiHelper.getUserDataResponse();
    }

}
