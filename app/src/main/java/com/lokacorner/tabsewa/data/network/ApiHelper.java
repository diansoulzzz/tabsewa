package com.lokacorner.tabsewa.data.network;

import com.lokacorner.tabsewa.data.network.model.BarangRequest;
import com.lokacorner.tabsewa.data.network.model.BarangResponse;
import com.lokacorner.tabsewa.data.network.model.BlogResponse;
import com.lokacorner.tabsewa.data.network.model.CategoryResponse;
import com.lokacorner.tabsewa.data.network.model.HomeCategoryResponse;
import com.lokacorner.tabsewa.data.network.model.HomeResponse;
import com.lokacorner.tabsewa.data.network.model.LoginRequest;
import com.lokacorner.tabsewa.data.network.model.LoginResponse;
import com.lokacorner.tabsewa.data.network.model.LogoutResponse;
import com.lokacorner.tabsewa.data.network.model.OpenSourceResponse;
import com.lokacorner.tabsewa.data.network.model.UserResponse;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface ApiHelper {

    ApiHeader getApiHeader();

    Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest request);

    Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest request);

    Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);

    Single<LogoutResponse> doLogoutApiCall();

    Single<BlogResponse> getBlogApiCall();

    Single<OpenSourceResponse> getOpenSourceApiCall();

    Single<BarangResponse.BarangListResponse> getBarangListResponse();

    Single<BarangResponse.BarangDataResponse> getBarangDataResponse(BarangRequest.BarangDataRequest request);

    Single<HomeResponse.HomeDataResponse> getHomeDataResponse();

    Single<CategoryResponse.CategoryDataResponse> getCategoryDataResponse();

    Single<HomeCategoryResponse.HomeCategoryDataResponse> getHomeCategoryDataResponse();

    Single<CategoryResponse.CategoryListResponse> getCategoryListResponse();

    Single<UserResponse.UserDataResponse> getUserDataResponse();

//    Single<BarangResponse.BarangDetailResponse> getBarangDetailResponse();
//    Single<UserDataResponse> getUserDataCall();
}
