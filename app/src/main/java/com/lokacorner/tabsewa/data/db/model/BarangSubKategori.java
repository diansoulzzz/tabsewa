package com.lokacorner.tabsewa.data.db.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangSubKategori {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("foto_url")
    @Expose
    private String fotoUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("barang_kategori_id")
    @Expose
    private Integer barangKategoriId;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("barang_kategori")
    @Expose
    private BarangKategori barangKategori;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getBarangKategoriId() {
        return barangKategoriId;
    }

    public void setBarangKategoriId(Integer barangKategoriId) {
        this.barangKategoriId = barangKategoriId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public BarangKategori getBarangKategori() {
        return barangKategori;
    }

    public void setBarangKategori(BarangKategori barangKategori) {
        this.barangKategori = barangKategori;
    }

    @Override
    public String toString() {
        return nama;
    }
}