package com.lokacorner.tabsewa.data.db;

import com.lokacorner.tabsewa.data.db.model.OptionOld;
import com.lokacorner.tabsewa.data.db.model.QuestionOld;
import com.lokacorner.tabsewa.data.db.model.UserOld;

import java.util.List;
import io.reactivex.Observable;


public interface DbHelper {

    Observable<Long> insertUser(final UserOld user);

    Observable<List<UserOld>> getAllUsers();

    Observable<List<QuestionOld>> getAllQuestions();

    Observable<Boolean> isQuestionEmpty();

    Observable<Boolean> isOptionEmpty();

    Observable<Boolean> saveQuestion(QuestionOld question);

    Observable<Boolean> saveOption(OptionOld option);

    Observable<Boolean> saveQuestionList(List<QuestionOld> questionList);

    Observable<Boolean> saveOptionList(List<OptionOld> optionList);
}
