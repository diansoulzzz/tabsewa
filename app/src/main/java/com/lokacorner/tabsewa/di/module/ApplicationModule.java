package com.lokacorner.tabsewa.di.module;

import android.app.Application;
import android.content.Context;

import com.lokacorner.tabsewa.BuildConfig;
import com.lokacorner.tabsewa.R;
import com.lokacorner.tabsewa.data.AppDataManager;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.data.db.AppDbHelper;
import com.lokacorner.tabsewa.data.db.DbHelper;
import com.lokacorner.tabsewa.data.network.ApiHeader;
import com.lokacorner.tabsewa.data.network.ApiHelper;
import com.lokacorner.tabsewa.data.network.AppApiHelper;
import com.lokacorner.tabsewa.data.prefs.AppPreferencesHelper;
import com.lokacorner.tabsewa.data.prefs.PreferencesHelper;
import com.lokacorner.tabsewa.di.AcceptHeader;
import com.lokacorner.tabsewa.di.ApiInfo;
import com.lokacorner.tabsewa.di.ApplicationContext;
import com.lokacorner.tabsewa.di.DatabaseInfo;
import com.lokacorner.tabsewa.di.PreferenceInfo;
import com.lokacorner.tabsewa.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @AcceptHeader
    String provideAcceptHeader() {
        return BuildConfig.ACCEPT_HEADER;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@AcceptHeader String accept,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(accept, preferencesHelper.getAccessToken());
    }

    @Provides
    @Singleton
    CalligraphyConfig provideCalligraphyDefaultConfig() {
        return new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/source-sans-pro/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build();
    }
}
