package com.lokacorner.tabsewa.di.component;

import com.lokacorner.tabsewa.di.PerService;
import com.lokacorner.tabsewa.di.module.ServiceModule;
import com.lokacorner.tabsewa.service.SyncService;

import dagger.Component;

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

}
