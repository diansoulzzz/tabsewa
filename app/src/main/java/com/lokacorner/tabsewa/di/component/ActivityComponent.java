package com.lokacorner.tabsewa.di.component;

import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.di.module.ActivityModule;
import com.lokacorner.tabsewa.ui.auth.AuthActivity;
import com.lokacorner.tabsewa.ui.auth.login.LoginFragment;
import com.lokacorner.tabsewa.ui.auth.loginold.LoginOldActivity;
import com.lokacorner.tabsewa.ui.auth.register.RegisterFragment;
import com.lokacorner.tabsewa.ui.barang_detail.BarangDetailActivity;
import com.lokacorner.tabsewa.ui.bayar_sewa.BayarSewaActivity;
import com.lokacorner.tabsewa.ui.empty.EmptyActivity;
import com.lokacorner.tabsewa.ui.entry_barang.EntryBarangActivity;
import com.lokacorner.tabsewa.ui.image_detail.ImageDetailActivity;
import com.lokacorner.tabsewa.ui.main.MainActivity;
import com.lokacorner.tabsewa.ui.main.account.AccountSourceFragment;
import com.lokacorner.tabsewa.ui.main.account.member.MemberSourceFragment;
import com.lokacorner.tabsewa.ui.main.account.vendor.VendorSourceFragment;
import com.lokacorner.tabsewa.ui.main.home.HomeSourceFragment;
import com.lokacorner.tabsewa.ui.main.kategori.KategoriSourceFragment;
import com.lokacorner.tabsewa.ui.main.search.SearchSourceFragment;
import com.lokacorner.tabsewa.ui.register_vendor.RegisterVendorActivity;
import com.lokacorner.tabsewa.ui.search.SearchActivity;
import com.lokacorner.tabsewa.ui.search_filter.SearchResultActivity;
import com.lokacorner.tabsewa.ui.splash.SplashActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(AuthActivity activity);

    void inject(BayarSewaActivity activity);

    void inject(ImageDetailActivity activity);

    void inject(SearchActivity activity);

    void inject(RegisterVendorActivity activity);

    void inject(EntryBarangActivity activity);

    void inject(EmptyActivity activity);

    void inject(LoginOldActivity activity);

    void inject(MainActivity activity);

    void inject(BarangDetailActivity activity);

    void inject(SplashActivity activity);

    void inject(HomeSourceFragment fragment);

    void inject(AccountSourceFragment fragment);

    void inject(SearchSourceFragment fragment);

    void inject(RegisterFragment fragment);

    void inject(LoginFragment fragment);

    void inject(MemberSourceFragment fragment);

    void inject(VendorSourceFragment fragment);

    void inject(KategoriSourceFragment fragment);

    void inject(SearchResultActivity activity);
}
