package com.lokacorner.tabsewa.di.component;

import android.app.Application;
import android.content.Context;

import com.lokacorner.tabsewa.MyApp;
import com.lokacorner.tabsewa.data.DataManager;
import com.lokacorner.tabsewa.di.ApplicationContext;
import com.lokacorner.tabsewa.di.module.ApplicationModule;
import com.lokacorner.tabsewa.service.SyncService;

import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyApp app);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}