package com.lokacorner.tabsewa.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

import com.lokacorner.tabsewa.data.db.model.Barang;
import com.lokacorner.tabsewa.data.db.model.BarangKategori;
import com.lokacorner.tabsewa.di.ActivityContext;
import com.lokacorner.tabsewa.di.PerActivity;
import com.lokacorner.tabsewa.ui.auth.AuthMvpPresenter;
import com.lokacorner.tabsewa.ui.auth.AuthMvpView;
import com.lokacorner.tabsewa.ui.auth.AuthPresenter;
import com.lokacorner.tabsewa.ui.auth.login.LoginMvpPresenter;
import com.lokacorner.tabsewa.ui.auth.login.LoginMvpView;
import com.lokacorner.tabsewa.ui.auth.login.LoginPresenter;
import com.lokacorner.tabsewa.ui.auth.loginold.LoginOldMvpPresenter;
import com.lokacorner.tabsewa.ui.auth.loginold.LoginOldMvpView;
import com.lokacorner.tabsewa.ui.auth.loginold.LoginOldPresenter;
import com.lokacorner.tabsewa.ui.auth.register.RegisterMvpPresenter;
import com.lokacorner.tabsewa.ui.auth.register.RegisterMvpView;
import com.lokacorner.tabsewa.ui.auth.register.RegisterPresenter;
import com.lokacorner.tabsewa.ui.barang_detail.BarangDetailMvpPresenter;
import com.lokacorner.tabsewa.ui.barang_detail.BarangDetailMvpView;
import com.lokacorner.tabsewa.ui.barang_detail.BarangDetailPresenter;
import com.lokacorner.tabsewa.ui.bayar_sewa.BayarSewaMvpPresenter;
import com.lokacorner.tabsewa.ui.bayar_sewa.BayarSewaMvpView;
import com.lokacorner.tabsewa.ui.bayar_sewa.BayarSewaPresenter;
import com.lokacorner.tabsewa.ui.empty.EmptyMvpPresenter;
import com.lokacorner.tabsewa.ui.empty.EmptyMvpView;
import com.lokacorner.tabsewa.ui.empty.EmptyPresenter;
import com.lokacorner.tabsewa.ui.entry_barang.EntryBarangMvpPresenter;
import com.lokacorner.tabsewa.ui.entry_barang.EntryBarangMvpView;
import com.lokacorner.tabsewa.ui.entry_barang.EntryBarangPresenter;
import com.lokacorner.tabsewa.ui.image_detail.ImageDetailMvpPresenter;
import com.lokacorner.tabsewa.ui.image_detail.ImageDetailMvpView;
import com.lokacorner.tabsewa.ui.image_detail.ImageDetailPresenter;
import com.lokacorner.tabsewa.ui.main.MainMvpPresenter;
import com.lokacorner.tabsewa.ui.main.MainMvpView;
import com.lokacorner.tabsewa.ui.main.MainPresenter;
import com.lokacorner.tabsewa.ui.main.account.AccountSourceMvpPresenter;
import com.lokacorner.tabsewa.ui.main.account.AccountSourceMvpView;
import com.lokacorner.tabsewa.ui.main.account.AccountSourcePresenter;
import com.lokacorner.tabsewa.ui.main.account.member.MemberSourceMvpPresenter;
import com.lokacorner.tabsewa.ui.main.account.member.MemberSourceMvpView;
import com.lokacorner.tabsewa.ui.main.account.member.MemberSourcePresenter;
import com.lokacorner.tabsewa.ui.main.account.vendor.VendorSourceMvpPresenter;
import com.lokacorner.tabsewa.ui.main.account.vendor.VendorSourceMvpView;
import com.lokacorner.tabsewa.ui.main.account.vendor.VendorSourcePresenter;
import com.lokacorner.tabsewa.ui.main.home.HomeSourceAdapter;
import com.lokacorner.tabsewa.ui.main.home.HomeSourceMvpPresenter;
import com.lokacorner.tabsewa.ui.main.home.HomeSourceMvpView;
import com.lokacorner.tabsewa.ui.main.home.HomeSourcePresenter;
import com.lokacorner.tabsewa.ui.main.kategori.KategoriSourceMvpPresenter;
import com.lokacorner.tabsewa.ui.main.kategori.KategoriSourceMvpView;
import com.lokacorner.tabsewa.ui.main.kategori.KategoriSourcePresenter;
import com.lokacorner.tabsewa.ui.main.search.SearchSourceAdapter;
import com.lokacorner.tabsewa.ui.main.search.SearchSourceMvpPresenter;
import com.lokacorner.tabsewa.ui.main.search.SearchSourceMvpView;
import com.lokacorner.tabsewa.ui.main.search.SearchSourcePresenter;
import com.lokacorner.tabsewa.ui.register_vendor.RegisterVendorMvpPresenter;
import com.lokacorner.tabsewa.ui.register_vendor.RegisterVendorMvpView;
import com.lokacorner.tabsewa.ui.register_vendor.RegisterVendorPresenter;
import com.lokacorner.tabsewa.ui.search.SearchMvpPresenter;
import com.lokacorner.tabsewa.ui.search.SearchMvpView;
import com.lokacorner.tabsewa.ui.search.SearchPresenter;
import com.lokacorner.tabsewa.ui.search_filter.SearchResultAdapter;
import com.lokacorner.tabsewa.ui.search_filter.SearchResultMvpPresenter;
import com.lokacorner.tabsewa.ui.search_filter.SearchResultMvpView;
import com.lokacorner.tabsewa.ui.search_filter.SearchResultPresenter;
import com.lokacorner.tabsewa.ui.splash.SplashMvpPresenter;
import com.lokacorner.tabsewa.ui.splash.SplashMvpView;
import com.lokacorner.tabsewa.ui.splash.SplashPresenter;
import com.lokacorner.tabsewa.utils.rx.AppSchedulerProvider;
import com.lokacorner.tabsewa.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    AuthMvpPresenter<AuthMvpView> provideAuthPresenter(
            AuthPresenter<AuthMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MemberSourceMvpPresenter<MemberSourceMvpView> provideMemberSourcePresenter(
            MemberSourcePresenter<MemberSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    VendorSourceMvpPresenter<VendorSourceMvpView> provideVendorSourcePresenter(
            VendorSourcePresenter<VendorSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    BayarSewaMvpPresenter<BayarSewaMvpView> provideBayarSewaPresenter(
            BayarSewaPresenter<BayarSewaMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegisterVendorMvpPresenter<RegisterVendorMvpView> provideRegisterVendorPresenter(
            RegisterVendorPresenter<RegisterVendorMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SearchMvpPresenter<SearchMvpView> provideSearchPresenter(
            SearchPresenter<SearchMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    EntryBarangMvpPresenter<EntryBarangMvpView> provideEntryBarangPresenter(
            EntryBarangPresenter<EntryBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ImageDetailMvpPresenter<ImageDetailMvpView> provideImageDetailPresenter(
            ImageDetailPresenter<ImageDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    EmptyMvpPresenter<EmptyMvpView> provideEmptyPresenter(
            EmptyPresenter<EmptyMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginOldMvpPresenter<LoginOldMvpView> provideLoginOldPresenter(
            LoginOldPresenter<LoginOldMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    BarangDetailMvpPresenter<BarangDetailMvpView> provideBarangDetailPresenter(
            BarangDetailPresenter<BarangDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    LoginMvpPresenter<LoginMvpView> providLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    RegisterMvpPresenter<RegisterMvpView> provideRegisterMvpPresenter(
            RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    SearchResultMvpPresenter<SearchResultMvpView> provideSearchResultMvpPresenter(
            SearchResultPresenter<SearchResultMvpView> presenter) {
        return presenter;
    }


    @Provides
    SearchResultAdapter.SearchAdapterBarang provideSearchResultAdapter() {
        return new SearchResultAdapter.SearchAdapterBarang(new ArrayList<Barang>());
    }

    @Provides
    HomeSourceMvpPresenter<HomeSourceMvpView> provideHomeSourceMvpPresenter(
            HomeSourcePresenter<HomeSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeSourceAdapter.HomeSourceAdapterBarang provideHomeSourceAdapterBarang() {
        return new HomeSourceAdapter.HomeSourceAdapterBarang(new ArrayList<Barang>());
    }

    @Provides
    HomeSourceAdapter.HomeSourceAdapterBarangKategori provideHomeSourceAdapterBarangKategori() {
        return new HomeSourceAdapter.HomeSourceAdapterBarangKategori(new ArrayList<BarangKategori>());
    }

    @Provides
    AccountSourceMvpPresenter<AccountSourceMvpView> provideAccountSourceMvpPresenter(
            AccountSourcePresenter<AccountSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    KategoriSourceMvpPresenter<KategoriSourceMvpView> provideKategoriSourceMvpPresenter(
            KategoriSourcePresenter<KategoriSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    SearchSourceMvpPresenter<SearchSourceMvpView> provideSearchSourceMvpPresenter(
            SearchSourcePresenter<SearchSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    SearchSourceAdapter.HomeSourceAdapterBarang provideSearchSourceAdapter() {
        return new SearchSourceAdapter.HomeSourceAdapterBarang(new ArrayList<Barang>());
    }

    @Provides
    SearchSourceAdapter.HomeSourceAdapterBarangKategori provideSearchSourceAdapterBarangKategori() {
        return new SearchSourceAdapter.HomeSourceAdapterBarangKategori(new ArrayList<BarangKategori>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager(AppCompatActivity activity) {
        return new GridLayoutManager(activity, 1);
    }
}
